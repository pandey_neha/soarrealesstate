<div class="" xmlns="http://www.w3.org/1999/html">
	<div id="slider" class="sl-slider-wrapper">
		<div class="sl-slider">
			<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25"
				 data-slice1-scale="2" data-slice2-scale="2">
				<div class="sl-slide-inner">
					<div class="bg-img bg-img-1"></div>
					<h2><?php echo $this->Html->link('2 Bed Rooms and 1 Dinning Room Aparment on Sale', array('controller' => 'homes', 'action' => '#')); ?></h2> <!--</a><a href="#">2 Bed Rooms and 1 Dinning Room Aparment on Sale</a></h2>-->
					<blockquote>
						<p class="location"><span class="glyphicon glyphicon-map-marker"></span> 1890 Syndey, Australia
						</p>

						<p>Until he extends the circle of his compassion to all living things, man will not himself find
							peace.</p>
						<cite>$ 20,000,000</cite>
					</blockquote>
				</div>
			</div>
			<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15"
				 data-slice1-scale="1.5" data-slice2-scale="1.5">
				<div class="sl-slide-inner">
					<div class="bg-img bg-img-2"></div>
					<h2><?php echo $this->Html->link('2 Bed Rooms and 1 Dinning Room Aparment on Sale', array('controller' => 'homes', 'action' => '#')); ?></h2>
					<blockquote>
						<p class="location"><span class="glyphicon glyphicon-map-marker"></span> 1890 Syndey, Australia
						</p>

						<p>Until he extends the circle of his compassion to all living things, man will not himself find
							peace.</p>
						<cite>$ 20,000,000</cite>
					</blockquote>
				</div>
			</div>
			<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3"
				 data-slice1-scale="2" data-slice2-scale="1">
				<div class="sl-slide-inner">
					<div class="bg-img bg-img-3"></div>
					<h2><?php echo $this->Html->link('2 Bed Rooms and 1 Dinning Room Aparment on Sale', array('controller' => 'homes', 'action' => '#')); ?></h2>
					<blockquote>
						<p class="location"><span class="glyphicon glyphicon-map-marker"></span> 1890 Syndey, Australia
						</p>

						<p>Until he extends the circle of his compassion to all living things, man will not himself find
							peace.</p>
						<cite>$ 20,000,000</cite>
					</blockquote>
				</div>
			</div>
			<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25"
				 data-slice1-scale="2" data-slice2-scale="1">
				<div class="sl-slide-inner">
					<div class="bg-img bg-img-4"></div>
					<h2><?php echo $this->Html->link('2 Bed Rooms and 1 Dinning Room Aparment on Sale', array('controller' => 'homes', 'action' => '#')); ?></h2>
					<blockquote>
						<p class="location"><span class="glyphicon glyphicon-map-marker"></span> 1890 Syndey, Australia
						</p>

						<p>Until he extends the circle of his compassion to all living things, man will not himself find
							peace.</p>
						<cite>$ 20,000,000</cite>
					</blockquote>
				</div>
			</div>
			<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10"
				 data-slice1-scale="2" data-slice2-scale="1">
				<div class="sl-slide-inner">
					<div class="bg-img bg-img-5"></div>
					<h2><?php echo $this->Html->link('2 Bed Rooms and 1 Dinning Room Aparment on Sale', array('controller' => 'homes', 'action' => '#')); ?></h2>
					<blockquote>
						<p class="location"><span class="glyphicon glyphicon-map-marker"></span> 1890 Syndey, Australia
						</p>

						<p>Until he extends the circle of his compassion to all living things, man will not himself find
							peace.</p>
						<cite>$ 20,000,000</cite>
					</blockquote>
				</div>
			</div>
		</div>
		<!-- /sl-slider -->
		<nav id="nav-dots" class="nav-dots">
			<span class="nav-dot-current"></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</nav>
	</div>
	<!-- /slider-wrapper -->
</div>
<div class="banner-search">
	<div class="container">
		<!-- banner -->
		<h3>Buy, Sale & Rent</h3>

		<div class="searchbar">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<?php	echo $this->Form->input('', array('type' => 'text', 'placeholder' => 'Search of Properties','class'=>'form-control'));?>
					<div class="row">
						<div class="col-lg-3 col-sm-3 ">
						<?php	echo $this->Form->select('field',array(
							'Value 1' => 'BUY',
							'Value 2' => 'SALE',
							'Value 3' => 'RENT'
							),array('class'=>'form-control'));?>
						</div>
						<div class="col-lg-3 col-sm-4">
							<?php	echo $this->Form->select('field',array(
									'Value 1' => '$150,000 - $200,000',
									'Value 2' => '$200,000 - $250,000',
									'Value 3' => '$250,000 - $300,000',
									'Value 4' => '$300,000 - above'
							),array('class'=>'form-control'));?>
						</div>
						<div class="col-lg-3 col-sm-4">
							<?php	echo $this->Form->select('field',array(
									'value 1'=>'selected=Property',
									'Value 2' => 'Property',
									'Value 3' => 'Apartment',
									'Value 4' => 'Building',
									'Value 5' => 'Office'
							),array('class'=>'form-control'));?>
						</div>
						<div class="col-lg-3 col-sm-4">
							<?php echo $this->Form->button('Find Now', array('type' => 'button','onclick'=>'window.location.href=\'/realestate/RealEstates/buy\';','class' => 'btn btn-success'));?>
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-lg-offset-1 col-sm-6 ">
					<p>Join now and get updated with all the properties deals.</p>
					<?php  $url = array();
					echo $this->Html->link(__("Sign In"), "#", array("data-toggle" => "modal", "data-target" => "#modal-static", "class" => "btn btn-info", "data-url" => "user/users/index"));
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- banner -->
<div class="container">
	<div class="properties-listing spacer"><?php echo $this->Html->link('View All Listing', array('controller' => 'RealEstates', 'action' => 'buy'),array('class'=>'pull-right viewall')); ?>
				<h2>Featured Properties</h2>

		<div id="owl-example" class="owl-carousel">
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/1.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
										<div class="status sold">Sold</div>
				</div>
				<h4><?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/2.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>

					<div class="status new">New</div>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/3.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/4.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/1.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>

					<div class="status sold">Sold</div>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/2.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
					<div class="status sold">Sold</div>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/3.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
					<div class="status new">New</div>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/4.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder">
					<?php echo $this->Html->image('../img/images/properties/1.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a<?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/2.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
			<div class="properties">
				<div class="image-holder"><?php echo $this->Html->image('../img/images/properties/3.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?>
				</div>
				<h4><a
					<?php echo $this->Html->link('Royal Inn', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
					</a></h4>
				<p class="price">Price: $234,900</p>

				<div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom"
												  data-original-title="Bed Room">5</span> <span data-toggle="tooltip"
																								data-placement="bottom"
																								data-original-title="Living Room">2</span>
					<span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span
							data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span></div>
				<a <?php echo $this->Html->link('View Details', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'btn btn-primary')); ?>
			</div>
		</div>
	</div>
	<div class="spacer">
		<div class="row">
			<div class="col-lg-6 col-sm-9 recent-view">
				<h3>About Us</h3>

				<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.
					Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in
					their exact original form, accompanied by English versions from the 1914 translation by H.
					Rackham.<br><a<?php echo $this->Html->link('Learn More', array('controller' => 'homes', 'action' => 'about')); ?></p>
			</div>
			<div class="col-lg-5 col-lg-offset-1 col-sm-3 recommended">
				<h3>Recommended Properties</h3>

				<div id="myCarousel" class="carousel slide">
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1" class=""></li>
						<li data-target="#myCarousel" data-slide-to="2" class=""></li>
						<li data-target="#myCarousel" data-slide-to="3" class=""></li>
					</ol>
					<!-- Carousel items -->
					<div class="carousel-inner">
						<div class="item active">
							<div class="row">

								<div class="col-lg-4"><?php echo $this->Html->image('../img/images/properties/1.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?></div>
								<div class="col-lg-8">
									<h5><a
										<?php echo $this->Html->link('Integer sed porta quam', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?></a></h5>

									<p class="price">$300,000</p>
									<a
									<?php echo $this->Html->link('MORE DETAIL', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'more')); ?></a></div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-lg-4"><?php echo $this->Html->image('../img/images/properties/2.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?></div>
								<div class="col-lg-8">
									<h5><a
										<?php echo $this->Html->link('Integer sed porta quam', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
										</a></h5>
									<p class="price">$300,000</p>
									<a
									<?php echo $this->Html->link('MORE DETAIL', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'more')); ?>
									</a></div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-lg-4"><?php echo $this->Html->image('../img/images/properties/3.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?></div>
								<div class="col-lg-8">
									<h5><a
										<?php echo $this->Html->link('Integer sed porta quam', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
										</a></h5>
									<p class="price">$300,000</p>
									<a
									<?php echo $this->Html->link('MORE DETAIL', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'more')); ?>
									</a></div>
							</div>
						</div>
						<div class="item">
							<div class="row">
								<div class="col-lg-4"><?php echo $this->Html->image('../img/images/properties/4.jpg' ,array('class'=>'img-responsive'), array('alt' => 'properties')); ?></div>
								<div class="col-lg-8">
									<h5><a
										<?php echo $this->Html->link('Integer sed porta quam', array('controller' => 'RealEstates', 'action' => 'property_detail')); ?>
										</a></h5>
									<p class="price">$300,000</p>
									<a
									<?php echo $this->Html->link('MORE DETAIL', array('controller' => 'RealEstates', 'action' => 'property_detail'),array('class'=>'more')); ?>
									</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
