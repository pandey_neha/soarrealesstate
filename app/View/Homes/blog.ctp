<!-- banner -->
<div class="inside-banner" xmlns="http://www.w3.org/1999/html">
		<div class="container">
			<span  <?php echo $this->Html->link('Home', array('controller' => 'homes', 'action' => 'index'),array('class'=>'pull-right')); ?>
			<h2>Blogs</h2>
		</div>
	</div>
	<!-- banner -->


	<div class="container">
		<div class="spacer blog">
			<div class="row">
				<div class="col-lg-8 col-sm-12 ">

					<!-- blog list -->
					<div class="row">
						<div class="col-lg-4 col-sm-4 "> <?php echo $this->Html->link(
							$this->Html->image('images/blog/4.jpg', array('alt' => 'blog title')),
							'blog_detail',
							array('escapeTitle' => false,'class'=>'thumbnail'));?>
							</div>
						<div class="col-lg-8 col-sm-8 ">
							<h3><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h3>
								<!--<a href="blog_detail">Creative business to takeover the market</a></h3>-->
							<div class="info">Posted on: Jan 20, 2013</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							  <?php echo $this->Html->link('Read More', array('controller' => 'homes', 'action' => 'blog_detail'),array('class'=>'more')); ?> <!--/ About Us
									href="blog_detail" class="more">Read More-->
						</div>
					</div>
					<!-- blog list -->
					<!-- blog list -->
					<div class="row">
						<div class="col-lg-4 col-sm-4 "><?php echo $this->Html->link(
									$this->Html->image('images/blog/5.jpg', array('alt' => 'blog title')),
									'blog_detail',
									array('escapeTitle' => false,'class'=>'thumbnail'));?>
						<!--	<a href="blog_detail" class="thumbnail">
								<img src="../img/images/blog/2.jpg" alt="blog title">
							</a>-->
						</div>
						<div class="col-lg-8 col-sm-8 ">
							<h3><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h3>
							<div class="info">Posted on: Jan 20, 2013</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<?php echo $this->Html->link('Read More', array('controller' => 'homes', 'action' => 'blog_detail'),array('class'=>'more')); ?>
						</div>
					</div>
					<!-- blog list -->
					<!-- blog list -->
					<div class="row">
					<div class="col-lg-4 col-sm-4">
						<?php
						 echo $this->Html->link(
								$this->Html->image('images/blog/1.jpg', array('alt' => 'blog title')),
								'blog_detail',
								array('escapeTitle' => false,'class'=>'thumbnail'));?>
					</div>
						<div class="col-lg-8 col-sm-8 ">
							<h3><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h3>
							<div class="info">Posted on: Jan 20, 2013</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<?php echo $this->Html->link('Read More', array('controller' => 'homes', 'action' => 'blog_detail'),array('class'=>'more')); ?>
						</div>
					</div>
					<!-- blog list -->
					<!-- blog list -->
					<div class="row">
						<div class="col-lg-4 col-sm-4 "><?php echo $this->Html->link($this->Html->image('images/blog/2.jpg', array('alt' => 'blog title')),'blog_detail',
									array('escapeTitle' => false,'class'=>'thumbnail'));?></div>
						<div class="col-lg-8 col-sm-8 ">
							<h3><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h3>
							<div class="info">Posted on: Jan 20, 2013</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<?php echo $this->Html->link('Read More', array('controller' => 'homes', 'action' => 'blog_detail'),array('class'=>'more')); ?>
						</div>
					</div>
					<!-- blog list -->
					<!-- blog list -->
					<div class="row">
						<div class="col-lg-4 col-sm-4 "><?php echo $this->Html->link($this->Html->image('images/blog/3.jpg', array('alt' => 'blog title')),'blog_detail',
									array('escapeTitle' => false,'class'=>'thumbnail'));?></div>
						<div class="col-lg-8 col-sm-8 ">
							<h3><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h3>
							<div class="info">Posted on: Jan 20, 2013</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							 <?php echo $this->Html->link('Read More', array('controller' => 'homes', 'action' => 'blog_detail'),array('class'=>'more')); ?>
						</div>
					</div>
					<!-- blog list -->
				</div>
				<div class="col-lg-4 visible-lg">
					<!-- tabs -->
					<div class="tabbable">
						<ul class="nav nav-tabs">
							<li class=""><?php echo $this->Html->link('Recent Post',array('controller'=>'homes','action'=>'#tab1'),array("data-toggle"=>"tab")); ?></li>
							<li class=""><?php echo $this->Html->link('Most Popular',array('controller'=>'homes','action'=>'#tab2'),array("data-toggle"=>"tab")); ?></li>
							<li class=""><?php echo $this->Html->link('Most Commented',array('controller'=>'homes','action'=>'#tab3'),array("data-toggle"=>"tab")); ?></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="tab1">
								<ul class="list-unstyled">
									<li>
										<h5><?php echo $this->Html->link('Real estate marketing growing', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
											<!--<a href="blog_detail">Real estate marketing growing</a></h5>-->
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
									<li>
										<h5><?php echo $this->Html->link('Real estate marketing growing', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
									<li>
										<h5><?php echo $this->Html->link('Real estate marketing growing', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane" id="tab2">
								<ul  class="list-unstyled">
									<li>
										<h5><?php echo $this->Html->link('Market update on new apartments', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<!--<h5><a href="blog_detail">Market update on new apartments</a></h5>-->
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
									<li>
										<h5><?php echo $this->Html->link('Market update on new apartments', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>

									<li>
										<h5><?php echo $this->Html->link('Market update on new apartments', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane active" id="tab3">
								<ul class="list-unstyled">
									<li>
										<h5><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
									<li>
										<h5><?php echo $this->Html->link('Creative business to takeover the market', array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
										<div class="info">Posted on: Jan 20, 2013</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- tabs -->
				</div>
			</div>
		</div>
	</div>
