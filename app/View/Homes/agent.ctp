	<!-- banner -->
	<div class="inside-banner">
		<div class="container">
			  <?php echo $this->Html->link('Home', array('controller' => 'homes', 'action' => 'index'),array('class'=>'pull-right')); ?>
			<h2>Agents</h2>
		</div>
	</div>
	<!-- banner -->
	<div class="container">
		<div class="spacer agents">

			<div class="row">
				<div class="col-lg-8  col-lg-offset-2 col-sm-12">
					<!-- agents -->
					<div class="row">
						<div class="col-lg-2 col-sm-2 "> <?php echo $this->Html->image('../img/images/agents/1.jpg' ,array('class'=>'img-responsive'), array('alt' => 'agent name')); ?></div>
						<div class="col-lg-7 col-sm-7 "><h4>John Smith</h4><p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p></div>
						<div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"> <?php  echo $this->Html->link('abc@realestete.com',array('controller'=>'homes','action'=>'mailto:abc@realestate.com','class'=>'glyphicon glyphicon-envelope'));?><br>
							<span class="glyphicon glyphicon-earphone"></span> (9009) 899 889</div>
					</div>
					<!-- agents -->

					<!-- agents -->
					<div class="row">
						<div class="col-lg-2 col-sm-2 "><?php echo $this->Html->image('../img/images/agents/2.jpg' ,array('class'=>'img-responsive'), array('alt' => 'agent name')); ?></div>
						<div class="col-lg-7 col-sm-7 "><h4>John Smith</h4><p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p></div>
						<div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span> <?php  echo $this->Html->link('abc@realestete.com',array('controller'=>'homes','action'=>'mailto:abc@realestate.com','class'=>'glyphicon glyphicon-envelope'));?><br>
							<span class="glyphicon glyphicon-earphone"></span> (9009) 899 889</div>
					</div>
					<!-- agents -->


					<!-- agents -->
					<div class="row">
						<div class="col-lg-2 col-sm-2 "> <?php echo $this->Html->image('../img/images/agents/3.jpg' ,array('class'=>'img-responsive'), array('alt' => 'agent name')); ?></div>
						<div class="col-lg-7 col-sm-7 "><h4>John Smith</h4><p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p></div>
						<div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span><?php  echo $this->Html->link('abc@realestete.com',array('controller'=>'homes','action'=>'mailto:abc@realestate.com','class'=>'glyphicon glyphicon-envelope'));?><br>
							<span class="glyphicon glyphicon-earphone"></span> (9009) 899 889</div>
					</div>
					<!-- agents -->
					<!-- agents -->
					<div class="row">
						<div class="col-lg-2 col-sm-2 "> <?php echo $this->Html->image('../img/images/agents/4.jpg' ,array('class'=>'img-responsive'), array('alt' => 'agent name')); ?></div>
						<div class="col-lg-7 col-sm-7 "><h4>John Smith</h4><p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p></div>
						<div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span>  <?php  echo $this->Html->link('abc@realestete.com',array('controller'=>'homes','action'=>'mailto:abc@realestate.com','class'=>'glyphicon glyphicon-envelope'));?><br>
							<span class="glyphicon glyphicon-earphone"></span> (9009) 899 889</div>
					</div>
					<!-- agents -->
				</div>
			</div>
		</div>
	</div>
