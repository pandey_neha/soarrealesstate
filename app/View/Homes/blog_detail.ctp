<!-- banner -->
<div class="inside-banner" xmlns="http://www.w3.org/1999/html">
	<div class="container">
		<?php echo $this->Html->link('Home', array('controller' => 'homes', 'action' => 'index'),array('class'=>'pull-right')); ?>
		<h2>Blogs</h2>
	</div>
</div>
<!-- banner -->
<div class="container">
	<div class="spacer blog">
		<div class="row">
			<div class="col-lg-8">
				<!-- blog detail -->
				<h2>Creative business to takeover the market</h2>
				<div class="info">Posted on: Jan 20, 2013 </div>
				<?php echo $this->Html->image('../img/images/blog/3.jpg' ,array('class'=>'thumbnail img-responsive'), array('alt' => 'blog title')); ?>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
					industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
					and scrambled it to make a type specimen book.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
					industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
					and scrambled it to make a type specimen book.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
					industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type
					and scrambled it to make a type specimen book.</p>
				<!-- blog detail -->
			</div>
			<div class="col-lg-4 visible-lg">
				<!-- tabs -->
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class=""><?php echo $this->Html->link('Recent Post',array('controller'=>'homes','action'=>'#tab1'),array("data-toggle"=>"tab")); ?></li>
						<li class=""><?php echo $this->Html->link('Most Popular',array('controller'=>'homes','action'=>'#tab2'),array("data-toggle"=>"tab")); ?></li>
						<li class=""><?php echo $this->Html->link('Most Commented',array('controller'=>'homes','action'=>'#tab3'),array("data-toggle"=>"tab")); ?></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane" id="tab1">
							<ul class="list-unstyled">
								<li>
									<h5><?php echo $this->Html->link('About',array('controller' => 'homes', 'action' => 'about')); ?></h5>
									 <?php echo $this->Html->link('Real estate marketing
											growing',array('controller' => 'homes', 'action' => 'blog_detail')); ?>
									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
								<li>
									<h5>  <?php echo $this->Html->link('Real estate marketing
											growing',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
								<li>
									<h5> <?php echo $this->Html->link('Real estate marketing
											growing',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
							</ul>
						</div>
						<div class="tab-pane" id="tab2">
							<ul class="list-unstyled">
								<li>
									<h5><?php echo $this->Html->link('Market update on new apartments',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
								<li>
									<h5><?php echo $this->Html->link('Market update on new apartments',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>

									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
								<li>
									<h5><?php echo $this->Html->link('Market update on new apartments',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>

									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
							</ul>
						</div>
						<div class="tab-pane active" id="tab3">
							<ul class="list-unstyled">
								<li>
									<h5><?php echo $this->Html->link('Creative business to takeover the market',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
								<li>
									<h5> <?php echo $this->Html->link('Creative business to takeover the market',array('controller' => 'homes', 'action' => 'blog_detail')); ?></h5>
									<div class="info">Posted on: Jan 20, 2013</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- tabs -->
			</div>
		</div>
	</div>
</div>
