<!DOCTYPE html>
<html>

<head>

	<title>Property Portal </title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<?php echo $this->Html->css('/assets/bootstrap/css/bootstrap.css');?>
	<?php echo $this->Html->css('/assets/style.css'); ?>
	<?php echo $this->Html->script('jquery-2.1.3.min.js'); ?>
	<?php echo $this->Html->script('/assets/bootstrap/js/bootstrap.js'); ?>
	<?php echo $this->Html->script('/assets/script.js'); ?>

	<!-- Owl stylesheet -->
	<?php echo $this->Html->css('/assets/owl-carousel/owl.carousel.css');?>
	<?php echo $this->Html->css('/assets/owl-carousel/owl.theme.css');?>
	<?php echo $this->Html->script('/assets/owl-carousel/owl.carousel.js');?>
<!-- slitslider -->
	<?php echo $this->Html->css('/assets/slitslider/css/style.css');?>
	<?php echo $this->Html->css('/assets/slitslider/css/custom.css');?>
	<?php echo $this->Html->script('/assets/slitslider/js/modernizr.custom.79639.js');?>
	<?php echo $this->Html->script('/assets/slitslider/js/jquery.ba-cond.min.js');?>
	<?php echo $this->Html->script('/assets/slitslider/js/jquery.slitslider.js');?>
	<?php $baseUrl = Router::url('/',true);
	?>
	<script> var HOST = '<?php echo $baseUrl; ?>'</script>
	<?php echo $this->Html->script('custom.js'); ?>
	<!-- slitslider -->

</head>
<body>
<?php echo $this->element('header');?>
<?php echo $this->fetch('content');?>
<?php echo $this->element('footer');?>
</body>
</html>
