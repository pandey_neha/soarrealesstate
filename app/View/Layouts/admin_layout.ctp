<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Property Portal</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta name="MobileOptimized" content="320">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <?php echo $this->html->css("/admin/assets/plugins/font-awesome/css/font-awesome.min.css");
    echo $this->html->css("/admin/assets/plugins/font-awesome/css/font-awesome.min.css");
    echo $this->html->css("/admin/assets/plugins/bootstrap/css/bootstrap.min.css");
    echo $this->html->css("/admin/assets/plugins/uniform/css/uniform.default.css"); ?>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <?php echo $this->html->css("/admin/assets/plugins/gritter/css/jquery.gritter.css");
    echo $this->html->css("/admin/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css");
    echo $this->html->css("/admin/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css");
    echo $this->html->css("/admin/assets/plugins/jqvmap/jqvmap/jqvmap.css");
    echo $this->html->css("/admin/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css"); ?>
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN THEME STYLES -->
    <?php echo $this->html->css("/admin/assets/css/style-metronic.css");
    echo $this->html->css("/admin/assets/css/style.css");
    echo $this->html->css("/admin/assets/css/style-responsive.css");
    echo $this->html->css("/admin/assets/css/plugins.css");
    echo $this->html->css("/admin/assets/css/pages/tasks.css");
    echo $this->html->css("/admin/assets/css/themes/default.css");
    echo $this->html->css("/admin/assets/css/custom.css");?>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="page-header-fixed">
<div>
    <?php echo $this->element('Admin/header'); ?>
</div>
<div class="clearfix"></div>
<div class="page-container">
<div>
    <?php echo $this->element('Admin/sidebar'); ?>
    <?php echo $this->fetch('content'); ?>
</div>
    <div>
       <?php //echo $this->element('Admin/page');?>
    </div>
    </div>
<div>
    <?php echo $this->element('Admin/footer'); ?>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]-->
<?php echo $this->Html->script("/admin/assets/plugins/respond.min.js");?>
<?php echo $this->Html->script("/admin/assets/plugins/excanvas.min.js");?>
<![endif]-->
<?php
echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js");
echo $this->Html->script("/admin/assets/plugins/jquery-migrate-1.2.1.min.js"); ?>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<?php echo $this->Html->script("/admin/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js");
echo $this->Html->script("/admin/assets/plugins/bootstrap/js/bootstrap.min.js");
echo $this->Html->script("/admin/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js");
echo $this->Html->script("/admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js");
echo $this->Html->script("/admin/assets/plugins/jquery.blockui.min.js");
echo $this->Html->script("/admin/assets/plugins/jquery.cookie.min.js");
echo $this->Html->script("/admin/assets/plugins/uniform/jquery.uniform.min.js") ?>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/jquery.vmap.js");
echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js");
echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js");
echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js");
echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js");
echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js");
echo $this->Html->script("/admin/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js");
echo $this->Html->script("/admin/assets/plugins/flot/jquery.flot.js");
echo $this->Html->script("/admin/assets/plugins/flot/jquery.flot.resize.js");
echo $this->Html->script("/admin/assets/plugins/jquery.pulsate.min.js");
echo $this->Html->script("/admin/assets/plugins/bootstrap-daterangepicker/moment.min.js");
echo $this->Html->script("/admin/assets/plugins/bootstrap-daterangepicker/daterangepicker.js");
echo $this->Html->script("/admin/assets/plugins/gritter/js/jquery.gritter.js");?>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<?php echo $this->Html->script("/admin/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js");
echo $this->Html->script("/admin/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js");
echo $this->Html->script("/admin/assets/plugins/jquery.sparkline.min.js");?>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php echo $this->Html->script("/admin/assets/scripts/app.js");
echo $this->Html->script("/admin/assets/scripts/index.js");
echo $this->Html->script("/admin/assets/scripts/tasks.js");?>
<!-- END PAGE LEVEL SCRIPTS -->
<?php echo $this->Html->script('custom.js'); ?>
<script>
    jQuery(document).ready(function () {
        App.init(); // initlayout and core plugins
        Index.init();
        Index.initJQVMAP(); // init index page's custom scripts
        Index.initCalendar(); // init index page's custom scripts
        Index.initCharts(); // init index page's custom scripts
        Index.initChat();
        Index.initMiniCharts();
        Index.initDashboardDaterange();
        Index.initIntro();
        Tasks.initDashboardWidget();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

