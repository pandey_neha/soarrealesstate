<div class="footer">

	<div class="container">


		<div class="row">
			<div class="col-lg-3 col-sm-3">
				<h4>Information</h4>
				<ul class="row">
					<li class="col-lg-12 col-sm-12 col-xs-3"><?php echo $this->Html->link('About', array('controller' => 'homes', 'action' => 'about')); ?></li>
					<li class="col-lg-12 col-sm-12 col-xs-3"><?php echo $this->Html->link('Agents', array('controller' => 'homes', 'action' => 'agent')); ?></li>
					<li class="col-lg-12 col-sm-12 col-xs-3"><?php echo $this->Html->link('Blog', array('controller' => 'homes', 'action' => 'blog')); ?></li>
					<li class="col-lg-12 col-sm-12 col-xs-3"><?php echo $this->Html->link('Contact', array('controller' => 'homes', 'action' => 'contact')); ?></li>
				</ul>
			</div>
			<div class="col-lg-3 col-sm-3">
				<h4>Newsletter</h4>

				<p>Get notified about the latest properties in our marketplace.</p>

				<form class="form-inline" role="form">
					<?php echo $this->Form->input('', array('type' => 'email', 'placeholder' => 'Enter your Email Address', 'class' => 'form-control')); ?>
					<?php echo $this->Form->button('Notify Me', array('type' => 'button', 'class' => 'btn btn-success')); ?>

				</form>
			</div>
			<div class="col-lg-3 col-sm-3">
				<h4>Follow us</h4>
				<?php
				echo $this->Html->link(
						$this->Html->image("images/facebook.png", array("alt" => "facebook")),
						"/homes",
						array('escape' => false)); ?>
				<?php
				echo $this->Html->link(
						$this->Html->image("images/twitter.png", array("alt" => "twitter")),
						"/homes",
						array('escape' => false)); ?>
				<?php
				echo $this->Html->link(
						$this->Html->image("images/linkedin.png", array("alt" => "linkedin")),
						"/homes",
						array('escape' => false)); ?>
				<?php
				echo $this->Html->link(
						$this->Html->image("images/instagram.png", array("alt" => "instagram")),
						"/homes",
						array('escape' => false)); ?>
			</div>
			<div class="col-lg-3 col-sm-3">
				<h4>Contact us</h4>

				<p><b>Bootstrap Realestate Inc.</b><br>
					<span class="glyphicon glyphicon-map-marker"></span> 8290 Walk Street, Australia <br>
					<span class="glyphicon glyphicon-envelope"></span> hello@bootstrapreal.com<br>
					<span class="glyphicon glyphicon-earphone"></span> (123) 456-7890</p>
			</div>
		</div>
		<p class="copyright">Copyright 2013. All rights reserved. </p>
	</div>
</div>
<!-- Modal -->
<div class="modal" data-backdrop="static" id="modal-static" tabindex="-1" role="dialog" aria-hidden="true"></div>
<div id="loginpop" class="modal fade ">
	<div id="loginpop" class="modal fade in" style="display: block;" aria-hidden="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="row">
				<div class="col-sm-6 login">
					<h4>Login</h4>
					<!--<form class="" role="form">-->
						<div class="form-group">
							<?php $url = array("controller" => 'users', "action" => "dashboard", 'plugin' => 'user');
							echo $this->Form->create("User", array("url" => $url, 'type' => 'post', "id" => "commonForm", "autocomplete" => "off",
									"inputDefaults" => array("label" => false, "div" => false), "novalidate" => true)); ?>
							<div class="col-sm-10">
								<?php echo $this->Form->input('', array('type' => 'email', 'label' => false, 'placeholder' => 'Enter your Email', 'required' => 'true', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10">
								<?php echo $this->Form->input('', array('type' => 'password', 'label' => false, 'placeholder' => 'Enter your Password', 'required' => 'true', 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-sm-8">

							<?php echo $this->Form->input('rememberMe', array('type' => 'checkbox', 'label' => 'Remember me')); ?>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<?php echo $this->Form->submit('Sign In', array('id' => 'login', 'class' => 'btn btn-primary', 'onclick' => 'login();')) ?>
							</div>
						</div>
						<?php echo $this->Form->end(); ?>
						<!--</form>-->
				</div>
				<div class="col-sm-6">
					<h4>New User Sign Up</h4>

					<p>Join today and get updated with all the properties deal happening around.</p>
					<?php echo $this->Html->Link('Join Now', array('controller' => 'users', 'action' => 'register', 'plugin' => 'user'), array('class' => 'btn btn-info')); ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
<!-- /.modal -->
