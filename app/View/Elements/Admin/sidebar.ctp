<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="page-sidebar-menu">
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li>
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <?php echo $this->Form->create('', array('url' => '', 'class' => 'sidebar-search')); ?>
            <div class="form-container">
                <div class="input-box">
                    <?php echo $this->Html->link('', array('controller' => '', 'action' => 'javascript:;'), array('class' => 'remove'));
                    echo $this->Form->input('', array('type' => 'text', 'placeholder' => "Search...", 'label' => false, 'div' => false));
                    echo $this->Form->submit('', array('type' => 'button', 'class' => 'submit', 'div' => false)); ?>
                </div>
            </div>
            <!--</form>-->
            <?php echo $this->Form->end(); ?>
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="start active ">

            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-home')) .
                $this->Html->tag('span', 'Dashboard', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'selected')),
                array('controller' => 'users', 'action' => 'dashboard','plugin'=>'user'),
                array('escape' => false)); ?>
        </li>
		<li class="profile">

			<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-user')) .
					$this->Html->tag('span', 'Profile', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'selected')),
					array('controller' => 'users', 'action' => 'profile','plugin'=>'user'),
					array('escape' => false)); ?>
		</li>
        <li class="update">
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-cogs')) .
                $this->Html->tag('span', 'Update Profile', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'arrow')),
                'javascript:;',
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li class="contact_detail">
                       <?php echo $this->Html->link("Contact Detail" .
                        $this->html->tag('span', '', array('class' => 'badge badge-roundless badge-important')),
                        array('controller' => 'users', 'action' => 'contact_detail','plugin'=>'user'),
                        array('escape' => false)); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Address Detail',
                        array('controller' => 'users', 'action' => 'address_detail','plugin'=>'user')); ?>
                </li>
                <!--<li>
                    <?php /*echo $this->Html->link('Horizontal Menu 1',
                        array('controller' => '', 'action' => 'layout_horizontal_menu1.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link('Horizontal Menu 2',
                        array('controller' => '', 'action' => 'layout_horizontal_menu2.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link('Disabled Menu Links',
                        array('controller' => '', 'action' => 'layout_disabled_menu.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link(' Sidebar Fixed Page',
                        array('controller' => 'admins', 'action' => 'layout_sidebar_fixed1')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link('Sidebar Closed Page',
                        array('controller' => '', 'action' => 'layout_sidebar_closed.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link(' Blank Page',
                        array('controller' => '', 'action' => 'layout_blank_page.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link(' Boxed Page',
                        array('controller' => '', 'action' => 'layout_boxed_page.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link('Non-Responsive Boxed Layout',
                        array('controller' => '', 'action' => 'layout_boxed_not_responsive.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link(' Promo Page',
                        array('controller' => '', 'action' => 'layout_promo.html')); */?>
                </li>
                <li>
                    <?php /*echo $this->Html->link(' Email Templates',
                        array('controller' => '', 'action' => 'layout_email.html')); */?>

                </li>
                <li>
                    <?php /*echo $this->Html->link('Content Loading via Ajax',
                        array('controller' => '', 'action' => 'layout_ajax.html')); */?>
                </li>-->
            </ul>
        </li>
        <li class="add_property" data-placement="left"
            data-original-title="Frontend&nbsp;Theme For&nbsp;Soar-Property&nbsp;Admin">
            <?php echo $this->Html->link(
                $this->Html->tag('i', '', array('class' => 'icon-gift')) . $this->Html->tag('span', 'Add Property', array('class' => 'title')),
                array('controller' => 'users', 'action' => 'add_property','plugin'=>'user'),
                array('escape' => false));
            ?>
        </li>
		<li class="property_list">

			<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-folder-open')) .
					$this->Html->tag('span', 'Property List', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'selected')),
					array('controller' => 'users', 'action' => 'property_list','plugin'=>'user'),
					array('escape' => false)); ?>
		</li>
        <li class="edit">
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-sitemap')) .
                $this->Html->tag('span', 'Edit Detail', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'arrow')),
                array('controller' => '', 'action' => ''),
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li>
                    <?php echo $this->Html->link('Edit Contact',
                        array('controller' => 'users', 'action' => 'edit_contact','plugin'=>'user')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Edit Address',
                        array('controller' => 'users', 'action' => 'edit_address','plugin'=>'user')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Typography',
                        array('controller' => '', 'action' => 'ui_typography.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Modals',
                        array('controller' => '', 'action' => 'ui_modals.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Extended Modals',
                        array('controller' => '', 'action' => 'ui_extended_modals.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Tabs, Accordions & Navs',
                        array('controller' => '', 'action' => 'ui_tabs_accordions_navs.html')); ?>

                </li>
                <li>
                    <?php echo $this->Html->link(' Tiles',
                        array('controller' => '', 'action' => 'ui_tiles.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('<span class="badge badge-roundless badge-warning">new</span>Toastr Notifications',
                        array('controller' => '', 'action' => 'ui_tiles.html'), array('escape' => false)); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Tree View',
                        array('controller' => '', 'action' => 'ui_tree.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Nestable List',
                        array('controller' => '', 'action' => 'ui_nestable.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('  <span class="badge badge-roundless badge-important">new</span>Ion Range Sliders',
                        array('controller' => '', 'action' => 'ui_nestable.html'), array('escape' => false)); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('<span class="badge badge-roundless badge-success">new</span>NoUI Range Sliders',
                        array('controller' => '', 'action' => 'ui_noui_sliders.html'),
                        array('escape' => false));
                    ?>
                </li>
                <li>
                    <?php echo $this->Html->link('jQuery UI Sliders',
                        array('controller' => '', 'action' => 'ui_jqueryui_sliders.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('jKnob Circle Dials',
                        array('controller' => '', 'action' => 'ui_knob.html')); ?>
                </li>
            </ul>
        </li>
        <li class="">
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-table')) .
                $this->Html->tag('span', 'Form Stuff', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'arrow')),
                array('controller' => '', 'action' => 'javascript:'),
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li>
                    <?php echo $this->Html->link('Form Controls',
                        array('controller' => '', 'action' => 'form_controls.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Form Layouts',
                        array('controller' => '', 'action' => 'form_layouts.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Form Component',
                        array('controller' => '', 'action' => 'form_component.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' <span class="badge badge-roundless badge-warning">new</span>Form X-editable',
                        array('controller' => '', 'action' => 'form_component.html'), array('escape' => false)); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Form Wizard',
                        array('controller' => '', 'action' => 'form_wizard.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Form Validation',
                        array('controller' => '', 'action' => 'form_validation.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('<span class="badge badge-roundless badge-important">new</span>Image Cropping',
                        array('controller' => '', 'action' => 'form_image_crop.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('Multiple File Upload',
                        array('controller' => '', 'action' => 'form_fileupload.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(' Dropzone File Upload',
                        array('controller' => '', 'action' => 'form_dropzone.html')); ?>
                </li>
            </ul>
        </li>
        <li class="">
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-sitemap')) .
                $this->Html->tag('span', 'Pages', array('class' => 'title')) .
                $this->Html->tag('span', '', array('class' => 'arrow')),
                array('controller' => '', 'action' => 'javascript:;'),
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li>
                    <?php echo $this->Html->link('<i class="icon-briefcase"></i>
                            <span class="badge badge-warning badge-roundless">new</span>Portfolio',
                        array('controller' => '', 'action' => 'page_portfolio.html'),
                        array('escape' => false)); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-time"></i>
                            <span class="badge badge-info">4</span>Timeline',
                        array('controller' => '', 'action' => 'page_timeline.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-cogs"></i>Coming Soon',
                        array('controller' => '', 'action' => 'page_coming_soon.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-comments"></i>Blog',
                        array('controller' => '', 'action' => 'page_blog.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-font"></i>Blog Posts',
                        array('controller' => '', 'action' => 'page_Blog_item.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-coffee"></i>
                        <span class="badge badge-success">9</span>Contact Us',
                        array('controller' => '', 'action' => 'page_news.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-bell"></i>News View',
                        array('controller' => '', 'action' => 'page_news_item.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-group"></i>About Us',
                        array('controller' => 'admins', 'action' => 'page_about'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-envelope-alt"></i>Contact Us',
                        array('controller' => '', 'action' => 'page_contact.html'),
                        array('escape' => false)); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('<i class="icon-calendar"></i>
                        <span class="badge badge-important">14</span>Calendar',
                        array('controller' => '', 'action' => 'page_calendar.html'),
                        array('escape' => false)); ?>


                </li>
            </ul>
        </li>
        <li class="">
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-gift')) .
                $this->Html->tag('span', 'Extra', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'arrow')),
                array('controller' => '', 'action' => ''),
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li>
                    <?php echo $this->Html->link('User Profile',
                        array('controller' => '', 'action' => 'extra_profile.html')); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('Lock Screen',
                        array('controller' => '', 'action' => 'extra_lock.html')); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('FAQ',
                        array('controller' => '', 'action' => 'extra_faq.html')); ?>

                </li>
                <li>
                    <!--                <a href="inbox.html">
                                        <span class="badge badge-important">4</span>Inbox</a>-->

                    <?php echo $this->Html->link('<span class="badge badge-important">4</span>Inbox',
                        array('controller' => '', 'action' => 'extra_search.html'), array('escape' => false)); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Search Results',
                        array('controller' => '', 'action' => 'extra_search.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('Invoice',
                        array('controller' => '', 'action' => 'extra_Invoice.html')); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('Pricing Tables',
                        array('controller' => '', 'action' => 'extra_pricing_table.html')); ?>

                </li>
                <li>
                    <?php echo $this->Html->link('404 Page Option 1',
                        array('controller' => '', 'action' => 'extra_404_option1.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('404 Page Option 2',
                        array('controller' => '', 'action' => 'extra_404_option2.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('404 Page Option 3',
                        array('controller' => '', 'action' => 'extra_404_option3.html')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link('500 Page Option 1',
                        array('controller' => '', 'action' => 'extra_500_option1.html')); ?>

                </li>
                <li>
                    <?php echo $this->Html->link(' 500 Page Option 2',
                        array('controller' => '', 'action' => 'extra_500_option2.html')); ?>
                </li>
            </ul>
        </li>
        <li>
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-leaf')) .
                $this->Html->tag('span', 'Level-Menu', array('class' => 'title')) . $this->Html->tag('span', '', array('class' => 'arrow')),
                array('controller' => '', 'action' => '', 'class' => 'active'),
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li>
                    <?php echo $this->Html->link('<span class="arrow"></span>Item 1',
                        array('controller' => '', 'action' => ''),
                        array('escape' => false)); ?>
                    <ul class="sub-menu">
                        <li>
                            <!--<a href="#">Sample Link 1</a>-->
                            <?php echo $this->Html->link('Sample Link 1',
                                array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li>
                            <!--  <a href="#">Sample Link 2</a>-->
                            <?php echo $this->Html->link('Sample Link 2',
                                array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li>
                            <!--<a href="#">Sample Link 3</a>-->
                            <?php echo $this->Html->link('Sample Link 3',
                                array('controller' => '', 'action' => '')); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <!--<a href="javascript:;">
                        Item 1
                        <span class="arrow"></span>
                    </a>-->
                    <?php echo $this->Html->link('<span class="arrow"></span>Item 2',
                        array('controller' => '', 'action' => ''),
                        array('escape' => false)); ?>
                    <ul class="sub-menu">
                        <li>
                            <!--     <a href="#">Sample Link 1</a>-->
                            <?php echo $this->Html->link('Sample Link 1',
                                array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li>
                            <!--<a href="#">Sample Link 1</a>-->
                            <?php echo $this->Html->link('Sample Link 1',
                                array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li>
                            <!-- <a href="#">Sample Link 1</a>-->
                            <?php echo $this->Html->link('Sample Link 1',
                                array('controller' => '', 'action' => '')); ?>
                        </li>
                    </ul>
                </li>
                <li>

                </li>
            </ul>
        </li>
        <li>
            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-folder-open')) .
                $this->Html->tag('span', '4 Level-Menu', array('class' => 'title')) .
                $this->Html->tag('span', '', array('class' => 'arrow')),
                array('controller' => '', 'action' => ''),
                array('escape' => false)); ?>
            <ul class="sub-menu">
                <li>
                    <?php echo $this->Html->link('<i class="icon-cogs"></i><span class="arrow"></span>Item 1',
                        array('controller' => '', 'action' => ''),
                        array('escape' => false)); ?>
                    <ul class="sub-menu">
                        <li>
                            <?php echo $this->Html->link('<i class="icon-user"></i><span class="arrow"></span>Sample Link 1',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>
                            <ul class="sub-menu">
                                <li>
                                    <?php echo $this->Html->link('<i class="icon-remove"></i> Sample Link 1',
                                        array('controller' => '', 'action' => ''),
                                        array('escape' => false)); ?>
                                </li>
                                <li><!--<a href="#"><i class="icon-pencil"></i> Sample Link 1</a>-->
                                    <?php echo $this->Html->link('<i class="icon-pencil"></i> Sample Link 2',
                                        array('controller' => '', 'action' => ''),
                                        array('escape' => false)); ?>
                                </li>
                                <li><!--<a href="#"><i class="icon-edit"></i> Sample Link 1</a>-->
                                    <?php echo $this->Html->link('<i class="icon-edit"></i> Sample Link 3',
                                        array('controller' => '', 'action' => ''),
                                        array('escape' => false)); ?>
                                </li>
                            </ul>
                        </li>
                        <li><!--<a href="#"><i class="icon-user"></i> Sample Link 2</a>-->
                            <?php echo $this->Html->link('<i class="icon-user"></i>Sample Link 2',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><i class="icon-external-link"></i> Sample Link 3</a>-->
                            <?php echo $this->Html->link('<i class="icon-external-link"></i> Sample Link 3',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><i class="icon-bell"></i> Sample Link 4</a>-->
                            <?php echo $this->Html->link('<i class="icon-bell"></i> Sample Link 4',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <!-- <a href="javascript:;">
                         <i class="icon-globe"></i>
                         Item 2
                         <span class="arrow"></span>
                     </a>-->
                    <?php echo $this->Html->link('<i class="icon-globe"></i><span class="arrow"></span>Item 2',
                        array('controller' => '', 'action' => 'javascript:;'),
                        array('escape' => false)); ?>

                    <ul class="sub-menu">
                        <li>
                            <!--<a href="#"><i class="icon-user"></i> Sample Link 1</a>-->
                            <?php echo $this->Html->link('<i class="icon-usre"></i> Sample Link 1',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>

                        </li>
                        <li>
                            <!--<a href="#"><i class="icon-external-link"></i> Sample Link 1</a>-->
                            <?php echo $this->Html->link('<i class="icon-external-link"></i> Sample Link 2',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>

                        </li>
                        <li>
                            <!--<a href="#"><i class="icon-bell"></i> Sample Link 1</a>-->
                            <?php echo $this->Html->link('<i class="icon-bell"></i> Sample Link 3',
                                array('controller' => '', 'action' => ''),
                                array('escape' => false)); ?>

                        </li>
                    </ul>
                </li>
                <li>
                    <!--<a href="#">
                        <i class="icon-folder-open"></i>
                        Item 3
                    </a>-->
                    <?php echo $this->Html->link('<i class="icon-folder-open"></i> Item 3',
                        array('controller' => '', 'action' => ''),
                        array('escape' => false)); ?>

                </li>
            </ul>
        </li>
        <li class="">
            <!--<a href="javascript:;">
                <i class="icon-user"></i>
                <span class="title">Login Options</span>
                <span class="arrow "></span>
            </a>-->
            <?php echo $this->Html->link('<i class="icon-user"></i><span class="title">Login Options</span>
                    <span class="arrow "></span>',
                array('controller' => '', 'action' => 'javascript:;'),
                array('escape' => false)); ?>

            <ul class="sub-menu">
                <li>
                    <!-- <a href="login.html">
                         Login Form 1</a>-->
                    <?php echo $this->Html->link('Login Form 1', array('controller' => '', 'action' => 'login.html')); ?>
                </li>
                <li>
                    <!--<a href="login_soft.html">
                        Login Form 2</a>
           -->         <?php echo $this->Html->link('Login Form 2', array('controller' => '', 'action' => 'login_soft.html')); ?>
                </li>
            </ul>
        </li>
        <li class="">
            <!-- <a href="javascript:;">
                 <i class="icon-th"></i>
                 <span class="title">Data Tables</span>
                 <span class="arrow "></span>
             </a>-->
            <?php echo $this->Html->link('<i class="icon-th"></i><span class="title">Data Tables</span>
                    <span class="arrow "></span>',
                array('controller' => '', 'action' => 'javascript:;'),
                array('escape' => false)); ?>

            <ul class="sub-menu">
                <li>
                    <!-- <a href="table_basic.html">
                         Basic Tables</a>-->
                    <?php echo $this->Html->link('Basic Tables',
                        array('controller' => '', 'action' => 'table_basic.html')); ?>
                </li>
                <li>
                    <!-- <a href="table_responsive.html">
                         Responsive Tables</a>-->
                    <?php echo $this->Html->link('Responsive Tables',
                        array('controller' => '', 'action' => 'table_responsive.html')); ?>

                </li>
                <li>
                    <!--  <a href="table_managed.html">
                          Managed Tables</a>-->
                    <?php echo $this->Html->link('Managed Tables',
                        array('controller' => '', 'action' => 'table_managed.html')); ?>
                </li>
                <li>
                    <!--   <a href="table_editable.html">
                           Editable Tables</a>-->
                    <?php echo $this->Html->link('Editable Tables',
                        array('controller' => '', 'action' => 'table_editable.html')); ?>

                </li>
                <li>
                    <!-- <a href="table_advanced.html">
                         Advanced Tables</a>-->
                    <?php echo $this->Html->link(' Advanced Tables',
                        array('controller' => '', 'action' => 'table_advanced.html')); ?>
                </li>
            </ul>
        </li>
        <li class="">
            <!-- <a href="javascript:;">
                 <i class="icon-file-text"></i>
                 <span class="title">Portlets</span>
                 <span class="arrow "></span>
             </a>-->
            <?php echo $this->Html->link('<i class="icon-file-text"></i><span class="title">Portlets</span>
                    <span class="arrow "></span>',
                array('controller' => '', 'action' => 'javascript:;'),
                array('escape' => false)); ?>

            <ul class="sub-menu">
                <li>
                    <!-- <a href="portlet_general.html">
                         General Portlets</a>-->
                    <?php echo $this->Html->link('General Portlets',
                        array('controller' => '', 'action' => 'portlet_general.html')); ?>
                </li>
                <li>
                    <!--<a href="portlet_draggable.html">
                        Draggable Portlets</a>-->
                    <?php echo $this->Html->link('Draggable Portlets',
                        array('controller' => '', 'action' => 'portlet_draggable.html')); ?>

                </li>
            </ul>
        </li>
        <li class="">
            <!--  <a href="javascript:;">
                  <i class="icon-map-marker"></i>
                  <span class="title">Maps</span>
                  <span class="arrow "></span>
              </a>-->
            <?php echo $this->Html->link('<i class="icon-map-marker"></i><span class="title">Maps</span>
                    <span class="arrow "></span>',
                array('controller' => '', 'action' => 'javascript:;'),
                array('escape' => false)); ?>

            <ul class="sub-menu">
                <li>
                    <!--<a href="maps_google.html">
                        Google Maps</a>-->
                    <?php echo $this->Html->link('Google Maps',
                        array('controller' => '', 'action' => 'maps_google.html')); ?>
                </li>
                <li>
                    <!--<a href="maps_vector.html">
                        Vector Maps</a>-->
                    <?php echo $this->Html->link('Vector Maps',
                        array('controller' => '', 'action' => 'maps_vector.html')); ?>
                </li>
            </ul>
        </li>
        <li class="last ">
            <!-- <a href="charts.html">
                 <i class="icon-bar-chart"></i>
                 <span class="title">Visual Charts</span>
             </a>-->
            <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-bar-chart')) .
                $this->html->tag('span', 'Visual Charts', array('class' => 'title')), array('controller' => '', 'action' => ''),
                array('escape' => false)); ?>

           <!-- --><?php /*echo $this->Html->link('<i class="icon-user"></i><span class="title">Visual Charts</span>',
                array('controller' => '', 'action' => 'charts.html'),
                array('escape' => false)); */?>

        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
