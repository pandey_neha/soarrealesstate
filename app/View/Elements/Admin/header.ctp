<div class="header navbar navbar-inverse navbar-fixed-top">
    <!--BEGIN TOP NAVIGATION BAR-->
    <div class="header-inner">
        <!--BEGIN LOGO-->

        <?php echo $this->Html->link($this->Html->image('/Admin/assets/img/logo.png', array('class' => 'img-responsive', 'alt' => 'log')),
            array('controller' => '', 'action' => 'index.html'), array('class' => 'navbar-brand', 'escape' => false)); ?>

        <!--   --><?php /*echo $this->Html->link($this->Html->image('/Admin/assets/img/menu-toggler.png'),
            array('controller' => '', 'action' => 'javascript:'), array('class' => 'nvbar-toggle', 'data-toggle' => 'collapse',
                'data-target' => '.navbar-collapse', 'escape' => false)); */ ?>

        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">
            <!-- BEGIN NOTIFICATION DROPDOWN -->
            <li class="dropdown" id="header_notification_bar">
                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-warning-sign')) .
                    $this->Html->tag('span', '6', array('class' => 'badge')),
                    array('controller' => '', 'action' => ''), array('class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown', 'data-hover' => 'dropdown',
                        'data-close-others' => 'true', 'escape' => false));
                ?>

                <ul class="dropdown-menu extended notification">
                    <li>
                        <p>You have 14 new notifications</p>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 250px;">
                            <li>


                                <!-- --><?php /*echo $this->Html->link('<span class="label label-sm label-icon label-success">
                                  <i class="icon-plus"></i></span>
                                   New user Registered.
                                  <span class="time">just now</span>',
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false));
                                */ ?>
                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-plus')),
                                        array('class' => 'label label-sm label-icon label-success')) . 'New user Registered' .
                                    $this->html->tag('span', 'Just Now', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>


                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bolt')),
                                        array('class' => 'label label-sm label-icon label-danger')) . ' Server #12 overloaded.' .
                                    $this->html->tag('span', '15 mins', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bell')),
                                        array('class' => 'label label-sm label-icon label-warning')) . ' Server #12 responding.' .
                                    $this->html->tag('span', '22 mins', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bullhorn')),
                                        array('class' => 'label label-sm label-icon label-info')) . 'Application error.' .
                                    $this->html->tag('span', '15 mins', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bolt')),
                                        array('class' => 'label label-sm label-icon label-danger')) . '  Database overloaded.68%' .
                                    $this->html->tag('span', '2hrs', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bolt')),
                                        array('class' => 'label label-sm label-icon label-danger')) . ' Server #12 overloaded.' .
                                    $this->html->tag('span', '5 hrs', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bell')),
                                        array('class' => 'label label-sm label-icon label-warning')) . 'Storage Server #4 not responding.' .
                                    $this->html->tag('span', '45 mins', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                            <li>


                                <?php echo $this->html->link($this->html->tag('span',
                                        $this->html->tag('i', '', array('class' => 'icon-bullhorn')),
                                        array('class' => 'label label-sm label-icon label-info')) . 'Syatem error' .
                                    $this->html->tag('span', '45 mins', array('class' => 'time')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>

                        </ul>
                    </li>
                    <li class="external">
                        <?php echo $this->Html->link('See all notifications', '<i class="m-icon-swapright"></i>',
                            array('controller' => '', 'action' => '')); ?>
                    </li>
                </ul>
            </li>
            <!-- END NOTIFICATION DROPDOWN -->
            <!-- BEGIN INBOX DROPDOWN -->
            <li class="dropdown" id="header_inbox_bar">

                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-envelope')) .
                    $this->Html->tag('span', '5', array('class' => 'badge')),
                    array('controller' => '', 'action' => ''), array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown',
                        'data-close-others' => 'true', 'escape' => false));
                ?>
                <ul class="dropdown-menu extended inbox">
                    <li>
                        <p>You have 12 new messages</p>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 250px;">
                            <li>
                                <?php echo $this->html->link($this->html->tag('span', $this->Html->image("/Admin/assets/img/avatar2.jpg"), array('class' => 'photo')) .
                                    $this->html->tag('span', $this->html->tag('span', 'Lisa Wong', array('class' => 'from')) .
                                        $this->html->tag('span', 'Just Now', array('class' => 'time')), array('class' => 'subject')) .
                                    $this->html->tag('span', 'Vivamus sed auctor nibh congue nibh. auctor nibh
                           auctor nibh...', array('class' => 'message')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span', $this->Html->image("/Admin/assets/img/avatar3.jpg"), array('class' => 'photo')) .
                                    $this->html->tag('span', $this->html->tag('span', 'Richard Doe', array('class' => 'from')) .
                                        $this->html->tag('span', '16 mins', array('class' => 'time')), array('class' => 'subject')) .
                                    $this->html->tag('span', 'Vivamus sed auctor nibh congue nibh. auctor nibh
                           auctor nibh...', array('class' => 'message')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span', $this->Html->image("/Admin/assets/img/avatar1.jpg"), array('class' => 'photo')) .
                                    $this->html->tag('span', $this->html->tag('span', ' Soar-Property', array('class' => 'from')) .
                                        $this->html->tag('span', '2 hrs', array('class' => 'time')), array('class' => 'subject')) .
                                    $this->html->tag('span', 'Vivamus sed auctor nibh congue nibh. auctor nibh
                           auctor nibh...', array('class' => 'message')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span', $this->Html->image("/Admin/assets/img/avatar2.jpg"), array('class' => 'photo')) .
                                    $this->html->tag('span', $this->html->tag('span', 'Lisa Wong', array('class' => 'from')) .
                                        $this->html->tag('span', '40 mins', array('class' => 'time')), array('class' => 'subject')) .
                                    $this->html->tag('span', ' Vivamus sed auctor 40% nibh congue nibh...', array('class' => 'message')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->html->link($this->html->tag('span', $this->Html->image("/Admin/assets/img/avatar3.jpg"), array('class' => 'photo')) .
                                    $this->html->tag('span', $this->html->tag('span', 'Richard', array('class' => 'from')) .
                                        $this->html->tag('span', '46 mins', array('class' => 'time')), array('class' => 'subject')) .
                                    $this->html->tag('span', ' Vivamus sed auctor 40% nibh congue nibh...', array('class' => 'message')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                            </li>
                        </ul>
                    </li>
                    <li class="external">
                        <?php echo $this->Html->link('See all messages.<i class="m-icon-swapright"></i>', array('controller' => '', 'action' => 'inbox'), array('escape' => false)); ?>
                    </li>
                </ul>
            </li>
            <!-- END INBOX DROPDOWN -->
            <!-- BEGIN TODO DROPDOWN -->
            <li class="dropdown" id="header_task_bar">

                <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-tasks')) .
                    $this->Html->tag('span', '7', array('class' => 'badge')),
                    array('controller' => '', 'action' => ''), array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown',
                        'data-close-others' => 'true', 'escape' => false)); ?>

                <ul class="dropdown-menu extended tasks">
                    <li>
                        <p>You have 12 pending tasks</p>
                    </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" style="height: 250px;">
                            <li>

                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'New release v1.2', array('class' => 'desc')) .
                                        $this->Html->tag('span', '30%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '40% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 40%;', 'class' => 'progress-bar progress-bar-success', 'aria-valuenow' => '40',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>


                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'Application deployment', array('class' => 'desc')) .
                                        $this->Html->tag('span', '65%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '65% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 65%;', 'class' => 'progress-bar progress-bar-danger', 'aria-valuenow' => '65',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress progress-striped')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>

                            </li>
                            <li>

                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'Mobile app release', array('class' => 'desc')) .
                                        $this->Html->tag('span', '98%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '19% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 98%;', 'class' => 'progress-bar progress-bar-success', 'aria-valuenow' => '98',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>

                            </li>
                            <li>

                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'Database Migration', array('class' => 'desc')) .
                                        $this->Html->tag('span', '10%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '10% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 10%;', 'class' => 'progress-bar progress-bar-warning', 'aria-valuenow' => '10',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress progress-striped')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>

                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'Web Server Upgrade', array('class' => 'desc')) .
                                        $this->Html->tag('span', '58%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '58% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 58%;', 'class' => 'progress-bar progress-bar-info', 'aria-valuenow' => '58',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress progress-striped')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>

                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'Mobile Development', array('class' => 'desc')) .
                                        $this->Html->tag('span', '85%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '85% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 85%;', 'class' => 'progress-bar progress-bar-success', 'aria-valuenow' => '85',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress progress-striped')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>

                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->tag('span',
                                        $this->Html->tag('span', 'New UI release', array('class' => 'desc')) .
                                        $this->Html->tag('span', '18%', array('class' => 'percent')),
                                        array('class' => 'task')) .
                                    $this->Html->tag('span', $this->Html->tag('span',
                                        $this->html->tag('span', '18% Complete', array('class' => 'sr-only')),
                                        array('style' => 'width: 18%;', 'class' => 'progress-bar progress-bar-important', 'aria-valuenow' => '18',
                                            'aria-valuemin' => '0', 'aria-valuemax' => '100')),
                                        array('class' => 'progress progress-striped')),
                                    array('controller' => '', 'action' => ''),
                                    array('escape' => false)); ?>
                            </li>
                        </ul>
                    </li>
                    <li class="external">
                        <?php echo $this->Html->link('See all tasks' . $this->Html->tag('i', '', array('class' => 'm-icon-swapright')),
                            array('controller' => '', 'action' => ''),
                            array('escape' => false)); ?>
                    </li>
                </ul>
            </li>
            <!-- END TODO DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <?php echo $this->html->link($this->Html->image("/Admin/assets/img/avatar1_small.jpg") . $this->html->tag('span', 'Soar-Property', array('class' => 'username')) .
                    $this->html->tag('i', '', array('class' => 'icon-angle-down')), array('controller' => '', 'action' => ''), array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown',
                    'data-hover' => 'dropdown',
                    'data-close-others' => 'true', 'escape' => false)); ?>
                <ul class="dropdown-menu">
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-user')) . 'My Profile',
                            array('controller' => '', 'action' => 'extra_profile.html'),
                            array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-calendar')) . 'My Calendar',
                            array('controller' => '', 'action' => 'page_calendar.html'),
                            array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-envelope')) . 'My Inbox' .
                            $this->Html->tag('span', '3', array('class' => 'badge badge-danger')),
                            array('controller' => '', 'action' => 'inbox.html'),
                            array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-tasks')) . 'My Tasks' .
                            $this->Html->tag('span', '7', array('class' => 'badge badge-success')),
                            array('controller' => '', 'action' => ''),
                            array('escape' => false)); ?>
                    </li>
                    <li class="divider"></li>
                    <li>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-lock')) . 'Change Password',
                            array('controller' => 'users', 'action' => 'change_password','plugin'=>'user'),
                            array('escape' => false)); ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-key')) . 'Log Out',
                            array('controller' => 'users', 'action' => 'logout','plugin'=>'user'),
                            array('escape' => false)); ?>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
