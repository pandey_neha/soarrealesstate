<!-- Header Starts -->
<div class="navbar-wrapper">
	<div class="navbar-inverse" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
						data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- Nav Starts -->
			<div class="navbar-collapse  collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><?php echo $this->Html->link('Home', array('controller' => 'homes', 'action' => 'index')); ?></li>
					<li><?php echo $this->Html->link('About', array('controller' => 'homes', 'action' => 'about')); ?></li>
					<li><?php echo $this->Html->link('Agents', array('controller' => 'homes', 'action' => 'agent')); ?></li>
					<li><?php echo $this->Html->link('Blog', array('controller' => 'homes', 'action' => 'blog')); ?></li>
					<li><?php echo $this->Html->link('Contact', array('controller' => 'homes', 'action' => 'contact')); ?></li>
				</ul>
			</div>
			<!-- #Nav Ends -->
		</div>
	</div>
</div>
<!-- #Header Starts -->
<div class="container">
	<!-- Header Starts -->
	<div class="header">
		<?php
		echo $this->Html->link(
				$this->Html->image("images/logo.png", array("alt" => "Realestate")),
				"/homes",
				array('escape' => false)); ?>
		<?php /*echo $this->Html->image("images/logo.png");*/ ?>
		<ul class="pull-right">
			<li><?php echo $this->Html->link('Buy', array('controller' => 'RealEstates', 'action' => 'buy')); ?></li>
			<li><?php echo $this->Html->link('Sale', array('controller' => 'RealEstates', 'action' => 'sale')); ?></li>
			<li><?php echo $this->Html->link('Rent', array('controller' => 'RealEstates', 'action' => 'rent')); ?></li>
		</ul>
	</div>
	<!-- #Header Starts -->
</div>
