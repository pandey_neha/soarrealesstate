<?php
/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 4/17/2015
 * Time: 11:11 AM
 */
class Contact extends AppModel {

	public $useDbConfig = 'index';
	public $useType = 'mytype';

	public $_mapping = array(
			'id' => array('type' => 'integer'),
			'name' => array('type' => 'string'),
			'number' => array('type' => 'string'),
			'special_type' => array(
					'type' => 'multi_field',
					'fields' => array(
							'not_analyzed' => array('type' => 'string', 'index' => 'not_analyzed'),
							'analyzed' => array('type' => 'string', 'index' => 'analyzed')
					)
			),
			'created' => array('type' => 'datetime'),
			'modified' => array('type' => 'datetime')
	);

	public function elasticMapping() {
		return $this->_mapping;
	}
}
