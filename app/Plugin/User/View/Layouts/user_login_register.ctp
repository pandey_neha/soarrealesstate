<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <?php
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    echo $this->Html->script('/packages/jquery/jquery.min');
    echo $this->Html->css("/packages/bootstrap/dist/css/bootstrap.min");
    echo $this->Html->css("/packages/components-font-awesome/css/font-awesome.min");
    echo $this->Html->css("/User/css/custom.css");
    ?>
</head>
<body>
<div class="container" id="container">
    <div class="row">
        <?php
        echo $this->Session->flash();
        echo $this->fetch('content');
        ?>
    </div>
</div>
</body>
</html>
