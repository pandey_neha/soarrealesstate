<div class="page-content">

	<div class="page-header" style="text-align: center " ><h2><?php echo __('Property List'); ?></h2></div>
<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('property_type'); ?></th>
		<th><?php echo $this->Paginator->sort('action'); ?></th>
		<th><?php echo $this->Paginator->sort('locality'); ?></th>
		<th><?php echo $this->Paginator->sort('country'); ?></th>
		<th><?php echo $this->Paginator->sort('state'); ?></th>
		<th><?php echo $this->Paginator->sort('city'); ?></th>
		<th><?php echo $this->Paginator->sort('area'); ?></th>
		<th><?php echo $this->Paginator->sort('unit'); ?></th>
		<th><?php echo $this->Paginator->sort('price'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($property as $propertys): ?>
		<tr>
			<td><?php echo $propertys['PropertyList']['id']; ?></td>
			<td><?php echo $propertys; ?></td>
		<!--	<td><?php /*echo $propertys['PropertyList']['id']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['property_type_id']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['action_id']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['locality']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['country_id']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['state_id']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['city_id']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['area']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['unit']; */?></td>
			<td><?php /*echo $propertys['PropertyList']['price']; */?></td>
			<td>-->
				<?php
				//echo $this->Html->link($propertys['PropertyList']['property_type_id'], array('controller' => 'users', 'action' => 'property_list','plugin'=>'user', $property['PropertyList']['id']));
				?>
			</td>
			<!--<td><?php /*echo $this->Time->nice($propertys['PropertyList']['action_id']); */?></td>-->
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<div>
	<?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
</div>
<div>
	<?php
/*	echo $this->Paginator->prev(__('< previous'), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ''));
	echo $this->Paginator->next(__('next >'), array(), null, array('class' => 'next disabled'));
	*/?>
</div>
</div>
</div>
<?php echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js");?>
<script>
	$('li').removeClass('active');
	if($('li').hasClass('property_list')) {
		$('.property_list').addClass('active');
		$('.property_list').children('a').append("<span class='selected'></span>");
	}

</script>
