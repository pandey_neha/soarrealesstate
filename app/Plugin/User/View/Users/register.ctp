<!-- banner -->
<div class="inside-banner">
	<div class="container">
		  <?php echo $this->Html->link('Home', array('controller' => 'homes', 'action' => 'index'),array('class'=>'pull-right')); ?>
		<h2>Register</h2>
	</div>
</div>
<!-- banner -->


<div class="container">
	<div class="spacer">
		<div class="row register">
			<div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">

				<?php echo $this->Session->flash(); ?>
				<?php echo $this->Form->create('User',array("name" => "registration", "action" => "register", "method" => "post",'class'=>'form-horizontal','inputDefaults'=>array('label'=>false)));?>
				<div class="form-group">
					<?php echo $this->Form->input('username', array('type' => 'text', 'label' => false, 'placeholder' => 'Username', 'required' => 'true', 'class' => 'form-control')); ?>
					<?php echo $this->Form->input('email_address', array('type'=>'email','label' => false,'placeholder' =>'E-mail','required' => 'true', 'unique'=>'true','class' => 'form-control')); ?>
					<?php echo $this->Form->input('password', array('type' => 'password', 'label' => false, 'placeholder' => 'Password', 'required' => 'true','class' => 'form-control')); ?>
					<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'label' => false, 'placeholder' => 'confirm_password', 'required' => 'true','class' => 'form-control')); ?>
					<?php echo $this->Form->input('address',array('type'=>'textarea','label'=>false,'placeholder'=>'Enter your Address','required'=>'true','class'=>'form-control'));?>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<?php echo $this->Form->submit('Register',array('class'=>'btn btn-primary'))?>
					</div>
				</div>
				<?php echo $this->Form->end();?>
			</div>
		</div>
	</div>
</div>
