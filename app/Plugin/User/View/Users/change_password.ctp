<div class="page-content">
<div id="page-wrap">
    <div id="wait" class="loader_postion">
        <?php echo $this->Html->image("/img/loading_blue.gif",array("class" => "loader_size padding_top_45"));?>
    </div>
    <div class="bg-color shadow">
        <div class=" margin-left-15 pull-left"><h4><?php echo __("Please Enter New Password") ?></h4></div>
        <div class="col-md-4 pull-right">
            <div class=" pull-right ">
                <?php if (!empty($message)) { ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span><span
                                class="sr-only"><?php echo __("Close"); ?></span>
                        </button>
                        <?php echo $message; ?>
                    </div>
                <?php } ?>

            </div>
        </div>
        <div class="clearfix margin-bottom-10"></div>
        <div class=" padding margin-bottom-20">
            <?php
            $url = array("controller" => "users", "action" => "change_password", "plugin" => "user");
            echo $this->Form->create("User", array("url" => $url, "id" => "commonForm", "class" => "form-horizontal", "autocomplete" => "off",
                "inputDefaults" => array("label" => false, "div" => false), "novalidate" => true)); ?>
            <div class="form-group">
                <label class="col-md-3 control-label"><?php echo __("Old Password:") ?></label>

                <div class="col-md-8">
                    <?php echo $this->Form->input("old_password", array("class" => "form-control", "type" => "password")); ?>
                </div>
                </div>
                <div class="col-md-1"></div>
			<div class="form-group">
                <div class=" clearfix margin-bottom-20"></div>
                <label class="col-md-3 control-label"><?php echo __("New Password:") ?></label>

                <div class="col-md-8">
                    <?php echo $this->Form->input("password", array("class" => "form-control", "type" => "password")); ?>
                </div>
                </div>
                <div class="col-md-1"></div>
                <div class=" clearfix margin-bottom-20"></div>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo __("Confirm Password:") ?></label>

                    <div class="col-md-8">
                        <?php echo $this->Form->input("confirm_password", array("class" => "form-control", "type" => "password")); ?>
                    </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="clearfix margin-bottom-20"></div>
                <div class="col-md-5"></div>
                <div class="col-md-1">
                    <?php echo $this->Form->submit(__("Submit"), array("class" => "btn btn-primary")); ?>
                    <div id="quote"></div>
                </div>
                <div class="col-md-5"></div>
                <div class=" clearfix margin-bottom-20"></div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="clearfix "></div>
        </div>
    </div>
</div>
</div>
