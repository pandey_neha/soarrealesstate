<div class="page-content">
	<?php echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js"); ?>

	<script type="text/javascript">
		$(document).ready(function () {
		});

		function state_fill(country_id) {
			url = "user/users/get_states/" + country_id;
			$.get(HOST + url, function (data) {
				state = JSON.parse(data);
				$("#state").empty();
				$.each(state, function (key, value) {
					$('#state').append($('<option>').text(value).attr('value', key));
				});
			});
		}
		function city_fill(state_id) {
			url = "user/users/get_cities/" + state_id;
			$.get(HOST + url, function (data) {
				cities = JSON.parse(data);
				$("#city").empty();
				$.each(cities, function (key, value) {
					$('#city').append($('<option>').text(value).attr('value', key));


				});
			});
		}
	</script>

	<h2><label class="row-md-5 control-label"><?php echo __("Add Property") ?></label></h2>

	<?php $url = array("controller" => "users", "action" => "add_property", "plugin" => "user");
	echo $this->Form->create("UserProperty", array("url" => $url, "method" => "post", "class" => "form-horizontal",
			"inputDefaults" => array("label" => false, "div" => false), "novalidate" => true)); ?>
	<div class="form-group">
		<div class=" clearfix margin-bottom-20"></div>
		<label class="col-md-4 control-label"><?php echo __("Property Type") ?></label>

		<div class="col-md-3">

			<?php
			echo $this->Form->select("property_type_id", $property_types, array('id' => 'type','empty' => 'Select Your Choice', "class" => "form-control")); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("Action") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('action_id', $actions, array('id' => 'action', 'empty' => 'Select Your Choice', 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class="form-group">
		<div class=" clearfix margin-bottom-20"></div>
		<label class="col-md-4 control-label"><?php echo __("Locality") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->input("locality", array("class" => "form-control", "type" => "text", 'required' => 'true')); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("Country") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('country_id', $countries, array('onChange' => 'state_fill(value)', 'id' => 'country', 'empty' => 'Select A Country', 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("State") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('state_id', null, array('onChange' => 'city_fill(this.value)', 'id' => 'state', 'empty' => 'Select A State', 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("City") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('city_id', null, array('id' => 'city', 'empty' => 'Select A City', 'class' => 'form-control')) ?>
		</div>
	</div>
	<div class="form-group">
		<div class=" clearfix margin-bottom-20"></div>
		<label class="col-md-4 control-label"><?php echo __("Area") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->input("area", array("class" => "form-control", "type" => "text", 'required' => 'true')); ?>
		</div>
	</div>
	<div class="form-group">
		<div class=" clearfix margin-bottom-20"></div>
		<label class="col-md-4 control-label"><?php echo __("Unit") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->input("unit", array("class" => "form-control", "type" => "text", 'required' => 'true')); ?>
		</div>
	</div>
	<div class="form-group">
		<div class=" clearfix margin-bottom-20"></div>
		<label class="col-md-4 control-label"><?php echo __("Price") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->input("price", array("class" => "form-control", "type" => "text", 'required' => 'true')); ?>
		</div>
	</div>
	<div class="clearfix margin-bottom-20"></div>
	<div class="col-md-4"></div>
	<div class="col-md-2">
		<?php echo $this->Form->submit(__("Add"), array("class" => "btn btn-primary")); ?>
	</div>
	<div class="col-md-5"></div>
	<div class=" clearfix margin-bottom-20"></div>
	<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js");?>
<script>
	$('li').removeClass('active');
	if($('li').hasClass('add_property')) {
		$('.add_property').addClass('active');
		$('.add_property').children('a').append("<span class='selected'></span>");
	}
	</script>
