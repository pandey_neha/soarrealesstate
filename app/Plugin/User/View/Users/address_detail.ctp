<div class="page-content">
	<?php echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js"); ?>
	<script type="text/javascript">
		$(document).ready(function () {
		});

		function state_fill(country_id) {
			url = "user/users/get_states/" + country_id;
			$.get(HOST + url, function (data) {
				state = JSON.parse(data);
				$("#state").empty();
				$.each(state, function (key, value) {
					$('#state').append($('<option>').text(value).attr('value', key));
				});
			});
		}
		function city_fill(state_id) {
			url = "user/users/get_cities/" + state_id;
			$.get(HOST + url, function (data) {
				cities = JSON.parse(data);
				$("#city").empty();
				$.each(cities, function (key, value) {
					$('#city').append($('<option>').text(value).attr('value', key));


				});
			});
		}
	</script>
	<h2><label class="row-md-5 control-label"><?php echo __("Address Detail") ?></label></h2>

	<?php $url = array("controller" => "users", "action" => "address_detail", "plugin" => "user");
	echo $this->Form->create("UserAddress", array("url" => $url, "method" => "post", "class" => "form-horizontal",
			"inputDefaults" => array("label" => false, "div" => false), "novalidate" => true)); ?>
	<div class="form-group">
		<div class=" clearfix margin-bottom-20"></div>
		<label class="col-md-4 control-label"><?php echo __("Address*") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->input("address", array("class" => "form-control", "type" => "text", 'required' => 'true')); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("Country*") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('country_id', $countries, array('onChange' => 'state_fill(value)', 'id' => 'country', 'empty' => 'Select A Country', 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("State*") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('state_id', null, array('onChange' => 'city_fill(this.value)', 'id' => 'state', 'empty' => 'Select A State', 'class' => 'form-control')); ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("City*") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->select('city_id', null, array('id' => 'city', 'empty' => 'Select A City', 'class' => 'form-control')) ?>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-4 control-label"><?php echo __("Zip Code*") ?></label>

		<div class="col-md-3">
			<?php echo $this->Form->input("zip_code", array("class" => "form-control", "type" => "text")); ?>
		</div>
	</div>
	<div class="clearfix margin-bottom-20"></div>
	<div class="col-md-4"></div>
	<div class="col-md-2">
		<?php echo $this->Form->submit(__("Update"), array("class" => "btn btn-primary")); ?>
	</div>
	<div class="col-md-5"></div>
	<div class=" clearfix margin-bottom-20"></div>
	<?php echo $this->Form->end(); ?>
</div>
<script>
	$('li').removeClass('active');
	if($('li').hasClass('update')) {
		$('.update').addClass('active');
		$('.update').children('a').append("<span class='selected'></span>");
	}
</script>
