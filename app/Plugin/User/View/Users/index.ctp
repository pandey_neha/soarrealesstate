<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header"
			<?php echo $this->Form->button('', array('class'=>'close' ,'data-dismiss'=>'model','aria-label'=>"Close",'type'=>'button'));?>
		<button class="close" aria-label="Close" data-dismiss="modal" type="button">
			<span aria-hidden="true">×</span>
		</button>

		<div class="row">
			<div class="col-sm-8 login">
				<h4>Login</h4>
				<!--<form class="" role="form">-->
					<?php echo $this->Session->flash('auth'); ?>
					<?php   $url = array("controller" => 'users', "action" => "login",'plugin'=>'user');
					echo $this->Form->create("User", array("url" => $url, 'type' => 'post', "id" => "commonForm",
							"inputDefaults" => array("label" => false, "div" => false), "novalidate" => true)); ?>
					<div class="col-sm-10">
						<?php echo $this->Form->input('email_address', array('type' => 'email', 'label' => false, 'placeholder' => 'Enter your Email', 'required' => 'true', 'class' => 'form-control')); ?>
					</div>
				<div class="form-group">
					<div class="col-sm-10">
						<?php echo $this->Form->input('password', array('type' => 'password', 'label' => false, 'placeholder' => 'Enter your Password', 'required' => 'true', 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="col-sm-10">

					<?php echo $this->Form->input(__('done'), array('hiddenField' => true, 'class' => 'rememberMeValue', "type" => "checkbox")); ?>
					<big><?php echo __("Remember Me"); ?></big>
					<div content="col-sm-6">
					<?php echo $this->Html->link('Forget  Password?',array('controller'=>'users','action'=>'remember_password','plugin'=>'user'));?>
</div>
					<?php /*echo $this->Form->input('rememberMe', array('type' => 'checkbox', 'label' => 'Remember me')); */?>
				</div>
				<div class="form-group">
					<div class="grid-col-sm-2">
						<?php echo $this->Form->submit('Sign In', array('class' => 'btn btn-primary'))?>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
					<!--</form>-->
			</div>
			<div class="grid-col-sm-4">
				<h4>New User Sign Up</h4>

				<p>Join today and get updated with all the properties deal happening around.</p>
				<?php echo $this->Html->Link('Join Now',array('controller'=>'users','action'=>'register','plugin'=>'user'),array('class' => 'btn btn-info'));?>
			</div>

			<div class="col-md-12">
				<div class="col-md-4" style="margin-top: 10px;">
					<?php echo $this->Html->link('Google', array('controller' => 'socials', 'action' => 'social_login','plugin'=>'user','Google'),array('class'=>'btn btn-primary')); ?>
				</div>
				<div class="padding-top:2px;">
					<div class="col-md-4" style="margin-top: 10px;">
						<?php echo $this->Html->link('Facebook', array('controller' => 'socials', 'action' => 'social_login','plugin'=>'user','Facebook'),array('class'=>'btn btn-primary')); ?>
					</div>
				</div>
				<div class="col-md-4" style="margin-top: 10px;">
					<?php echo $this->Html->link('Twitter', array('controller' => 'socials', 'action' => 'social_login','plugin'=>'user','Twitter'),array('class'=>'btn btn-primary')); ?>
				</div>
			</div>

		</div>

		</div>

</div>
</div>
