<div class="users form">
	<?php
	$url = array('controller' => 'users', 'action' => 'forgot_password');
	echo $this->Form->create('User',array('url' => $url)); ?>
	<fieldset>
		<legend><?php echo __('Forgot Password'); ?></legend>

		<div style="text-align:justify; line-height:21px;">
			Please use  your registered email id to recover password. An email will be sent to this email id regarding recovering password.
		</div>
		<?php if(!empty($message)){ ?> <div class="message_box"><?php echo $message ;?></div> <?php } ?>
       <?php
             echo $this->Form->input('email_address',array('label'=>'email','type'=>'email','placeholder'=>'enter your email address',
		       'required'=>1,'unique'=>0,'pattern'=>"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{3,4}$"));?>
	   <div class="form-group">
					<div class=" col-sm-1">
	   <?php echo $this->Form->submit('ok', array('class' => 'form-submit',  'title' => 'Click here to get the password','class'=>'btn btn-primary') );

	  // echo $this->Form->submit('ok', array('class' => 'form-submit',  'title' => 'Click here to get the password') );

			?>
	</fieldset>
<?php echo $this->Form->end(); ?>
	</div>
</div>
<?php
echo $this->Html->link( "return to login",array('controller'=>'homes','action'=>'index','plugin'=>'') );
?>
