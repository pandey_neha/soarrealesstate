<?php __('Forget Password'); ?></h2>
<div class="User form">
	<?php echo $this->Form->create('User', array('action' => 'remember_password')); ?>
	<?php echo $this->Form->input('email')?>
	<div class="col-sm-1">
	<?php echo $this->Form->submit('Recover',array('controller'=>'users','action'=>'remember_password','plugin'=>'user','class' => 'btn btn-primary'));?>
	</div>
	<?php echo $this->Form->end();?>
</div>
