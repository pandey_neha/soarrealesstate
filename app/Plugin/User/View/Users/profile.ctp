<div class="page-content">

	<h1>My Profile</h1>
	<?php $url = array("controller" => "users", "action" => "profile", "plugin" => "user");
	echo $this->Form->create("User", array("url" => $url)); ?>

	<div class="form-group">
		<!--<div class=" clearfix margin-bottom-20"></div>-->
		<label class="col-md-2 control-label"><?php echo __("Username") ?></label>

		<div class="col-lg-10">
			<?php	//echo $this->Form->hidden('id', array('value' =>$users['id']));?>
			<p class="form-control-static" ><?php echo $users['User']['username']; ?></p>
		</div>
</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Email Address") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"> <?php echo $users['User']['email_address']; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Address") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"> <?php echo $users['User']['address']; ?></p>
		</div>
	</div>


	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Contact 1") ?></label>

		<div class="col-md-10">
			<p class="form-control-static">	<?php echo $users['UserContact']['contact_1']; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Contact 2") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"><?php echo $users['UserContact']['contact_2']; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Contact 3") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"><?php echo $users['UserContact']['contact_3']; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Zip Code") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"><?php echo $users['UserAddress']['zip_code']; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("Country") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"><?php echo $country; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("State") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"><?php echo $state; ?></p>
		</div>
	</div>
	<div class=" clearfix margin-bottom-20"></div>
	<div class="form-group">
		<label class="col-md-2 control-label"><?php echo __("City") ?></label>

		<div class="col-md-10">
			<p class="form-control-static"><?php echo $city; ?></p>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js");?>
<script>
	$('li').removeClass('active');
	if($('li').hasClass('profile')) {
		$('.profile').addClass('active');
		$('.profile').children('a').append("<span class='selected'></span>");
	}
</script>
