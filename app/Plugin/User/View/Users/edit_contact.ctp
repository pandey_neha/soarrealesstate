<div class="page-content">
	<div class="bg-color shadow">
		<label class="row-md-5 control-label">
			<h2>Add Contact</h2></label>
		<!--<h2><label class="row-md-5 control-label"><?php /*echo __("Contact Detail") */?></label></h2>-->
		<div class="">
			<div style="text-align:justify; line-height:40px;">
				This is where people interested in your property will contact you
			</div>
		</div>
		<div class="clearfix margin-bottom-10"></div>
		<div class=" padding margin-bottom-20">
			<?php echo $this->Session->flash(); ?>
			<?php
			$url = array("controller" => "users", "action" => "edit_contact", "plugin" => "user");
			echo $this->Form->create("UserContact", array("url" => $url, "method" => "post", "class" => "form-horizontal",
					"inputDefaults" => array("label" => false, "div" => false), "novalidate" => true)); ?>
			<div class="form-group">
				<div class=" clearfix margin-bottom-20"></div>
				<label class="col-md-3 control-label"><?php echo __("Contact no.1") ?></label>

				<div class="col-md-9">
					<?php echo $this->Form->input("contact_1", array("class" => "form-control", "type" => "text",'required' =>'true')); ?>
				</div>
			</div>
			<div class=" clearfix margin-bottom-20"></div>
			<div class="form-group">
				<label class="col-md-3 control-label"><?php echo __("Contact no.2") ?></label>

				<div class="col-md-9">
					<?php echo $this->Form->input("contact_2", array("class" => "form-control", "type" => "text",'required' => 'true')); ?>
				</div>
			</div>
			<div class=" clearfix margin-bottom-20"></div>
			<div class="form-group">
				<label class="col-md-3 control-label"><?php echo __("Contact no.3") ?></label>

				<div class="col-md-9">
					<?php echo $this->Form->input("contact_3", array("class" => "form-control", "type" => "text")); ?>
				</div>
			</div>
			<div class="clearfix margin-bottom-20"></div>
			<div class="col-xs-4"></div>
			<div class="col-xs-1">
				<?php echo $this->Form->submit(__("Add"), array("class" => "btn btn-primary")); ?>
				<div id="quote"></div>
			</div>
			<div class="col-xs-5"></div>
			<div class=" clearfix margin-bottom-20"></div>
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="clearfix "></div>
	</div>
</div>
<?php echo $this->Html->script("/admin/assets/plugins/jquery-1.10.2.min.js");?>
<script>
	$('li').removeClass('active');
	if($('li').hasClass('edit')) {
		$('.edit').addClass('active');
		$('.edit').children('a').append("<span class='selected'></span>");
	}
</script>

