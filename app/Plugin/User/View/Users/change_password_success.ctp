<div id="page-wrap">
    <div id="wait" class="loader_postion">
        <?php echo $this->Html->image("/img/loading_blue.gif",array("class" => "loader_size padding_top_38"));?>
    </div>
    <div class="bg-color shadow" >
        <div class="pull-right">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
        </div>
        <div class="clearfix"></div>
        <div class="success_message">
            <?php echo __("Your Password Is Changed Successfully. Please Use Your New Password To Login Again!");
			//echo $this->Html->link(__("Log In"), array('controller'=>'homes','action'=>'index'),array("class"=>"btn btn-primary"));
			?>
        </div>
        <div class="text-center padding_bottom_40">
			<div class="col-sm-1">
            <?php  $url = array();
			echo $this->Html->link(__("Log In"), array('controller'=>'homes','action'=>'index','plugin'=>''),array('class'=>'btn btn-primary'));
            ?>
        </div>
		</div>
    </div>
</div>
