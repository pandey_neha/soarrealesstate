<?php
/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 4/1/2015
 * Time: 10:22 AM
 */
App::uses('AuthComponent', 'Controller/Component');
class UserContact extends UserAppModel
{
	//public $name = "UserContact";
	var $useTable = "user_contacts";
	public $recursive = 1;
	public $belongsTo = 'User';
public $validate = array('contact_1' => array(
		'notEmpty' => array(
				'rule' => 'numeric',
				'required' => 'true',
				'message' => 'Please Enter valid Phone No.',
				"last" => true
		),
		'minLength' => array(
				'rule' => array('minLength', 10),
				'message' => 'Minimum 10 Digit long')
),
		'contact_2' => array(
				'notEmpty' => array(
						'rule' => 'notEmpty',
						'required' => 'true',
						'message' => 'Please Enter valid Phone No.',
						"last" => true
				),
				'minLength' => array(
						'rule' => array('minLength', 10),
						'message' => 'Minimum 10 Digit long')
		),
);
	/*public $validates = array('contact_1' =>array(
'rule' => 'numeric',
'allowEmpty' => true, //validate only if not empty
'message'=>'Phone number should be numeric',

			'minLength' => array(
							'rule' => array('minLength', 10),
							'message' => 'Minimum 10 Digit long')
	),

			'contact_2' => array(
							'rule' => 'numeric',
							'allowEmpty' => true,
							'message' => 'Please Enter Your correct No.',

					'minLength' => array(
							'rule' => array('minLength', 10),
							'message' => 'Minimum 10 Digit long')
			),

	);*/
}
