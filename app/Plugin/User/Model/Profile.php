<?php
/**
 * Created by IntelliJ IDEA.
 * User: nithyapasam
 * Date: 9/30/14
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class Profile extends UserAppModel
{
    public $name = "Profile";

    //public $belongsTo = array('User.Profile'); //associate users and groups to rows in the Acl tables.it requires use of parentNode()

    public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

    public $primaryKey = "id";

    public function parentNode()
    {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($data)) {
            $data = $this->read();
        }
        if (!$data["Group"]["group_id"]) {
            return null;
        } else {
            return array('Group' => array('id' => $data["Group"]["group_id"]));
        }
    }


    public $validate = array(

        "profile_image" => array(
            "image" => array(
                "rule" => array("extension", array("gif", "jpeg", "png", "jpg")),
                "message" => "Upload valid image.",
                "last" => true
            ),
            "size" => array(
                "rule" => array("filesize", "<=", "1MB"),
                "message" => "Image must be greater than or equal to 1MB",
                "last" => true
            )
        )
    );

    public function updateProfileImage($profileImage, $userId, $profileId)
    {
        if (!empty($userId) && !empty($profileImage)) {
            //$list = $this->find("list", array("conditions" => array("user_id" => $userId), "fields" => "Profile.id"));
            if (!empty($profileId)) {
                //update
                $this->id = $profileId;
                return $this->saveField("profile_image", $profileImage);
            } else {
                $data["Profile"]["profile_image"] = $profileImage;
                $data["Profile"]["user_id"] = $userId;
                return $this->save($data, false);
            }
        }
        return false;
    }

    public function getProfileDataByUserId($userId)
    {
        if (!empty($userId)) {
            $conditions = array("Profile.user_id" => $userId);
            $result = $this->find("first", array("conditions" => $conditions));
            return $result;
        }
        return null;
    }

    public function getProfileImageById($userId)
    {
        $list = $this->find("list", array("conditions" => array("user_id" => $userId), "fields" => "Profile.profile_image"));
        return array_shift($list);
    }

    public function getIdByEmailAddress($emailAddress)
    {
        if (!empty($emailAddress)) {
            $fields = array("User.id");
            $list = $this->find("list", array("conditions" => array("email_address" => $emailAddress), "fields" => $fields));
            return array_shift($list);
        }
        return null;
    }

}
