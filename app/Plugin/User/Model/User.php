<?php
/**
 * Created by IntelliJ IDEA.
 * User: nithyapasam
 * Date: 9/19/14
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('AuthComponent', 'Controller/Component');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends UserAppModel
{
	public $name = "User";

	public $primaryKey = 'id';
	public $recursive = 1;
public $hasOne=array('UserContact'=>array('className'=>'UserContact'),'UserAddress'=>array('className'=>'UserAddress'));
	public $hasMany = array(
			'SocialProfile' => array(
					'className' => 'SocialProfile',
			)
			/*'SiteUrl'=>array(
					'className'=> 'Editor.SiteUrl'
			)*/
	);


	/* public function parentNode()
        {
            if (!$this->id && empty($this->data)) {
                return null;
            }
            $data = $this->data;
            if (empty($data)) {
                $data = $this->read();
            }
            if (!$data[$this->alias]["group_id"]) {
                return null;
            } else {
                return array('Group' => array('id' => $data[$this->alias]["group_id"]));
            }
        }*/

	public $validate = array('username' => array(
			'notEmpty' => array(
					'rule' => 'notEmpty',
					'required' => 'true',
					'message' => 'Please Enter Username',
					"last" => true
			)
	),

			"email_address" => array(
					"notBlank" => array(
							"rule" => "notEmpty",
							"message" => "This field must not be blank.",
							"last" => true
					),
					"valid_email" => array(
							"rule" => "email",
							"message" => "Please enter proper email address.",
							"last" => true
					),
					 "check_email" => array(
                "rule" => "checkEmailAddress",
                "message" => "This User does not exists"
            )

			),/*
			'password' => array(
					'notEmpty' => array(
							'rule' => 'notEmpty',
							'required' => 'true',
							'message' => 'Please Enter Password',
							"last" => true
					),
					'minLength' => array(
							'rule' => array('minLength', 8),
							'message' => 'Minimum 8 characters long'
					)
			),
			'confirm_password' => array(
					'required' => array(
							'rule' => array('notEmpty'),
							'message' => 'Please confirm your password'
					),
					'equaltofield' => array(
							'rule' => array('equaltofield', 'password'),
							'message' => 'Both passwords must match.'
					)
			),*/
	);



	public function validateNewUser()
	{
		$validateNewUser = array(
				"email_address" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "This field must not be blank.",
								"last" => true
						),
						"valid_email" => array(
								'required' => 'true',
								"rule" => array('email',true),
								"message" => "Please enter proper email address.",
								"last" => true
						),
						"unique_email" => array(
								"rule" => "isUnique",
								"message" => "Entered email address already exists. Please fill another email address."
						)

				),
				"password" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "password field must not be blank.",
								"last" => true
						),
						"minLength" => array(
								"rule" => array("minLength", "8"),
								"message" => "Minimum 8 characters long.",

						)
				)
		);
		$this->validate = $validateNewUser;
		return $this->validates();
	}

	public function validateExistingUser()
	{
		$validateExistingUser = array(
				"email_address" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "This field must not be blank.",
								"last" => true
						),
						"valid_email" => array(
								"rule" => "email",
								"message" => "Please enter proper email address.",
								"last" => true
						),
						"valid_user" => array(
								"rule" => "validateUser",
								"message" => "Please fill proper email address and password.",
						),
				),
				"password" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "password field must not be blank.",
								"last" => true
						),
						"minLength" => array(
								"rule" => array("minLength", "8"),
								"message" => "Minimum 8 characters long.",

						)
				)
		);
		$this->validate = $validateExistingUser;
		return $this->validates();
	}
	function get_user_detail($email)
	{
		$user = $this->find("first", array('conditions' => array('email_address' => $email)));
		return $user;
	}

	public function validateVerificationCode()
	{
		$validateVerification = array(
				"code" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "This field must not be blank.",
								"last" => true
						),
						"valid_code" => array(
								"rule" => "validateCode",
								"message" => "Please fill correct verification code.",
						),

				)
		);
		$this->validate = $validateVerification;
		return $this->validates();
	}

	public function validatePassword()
	{
		$validatePassword = array(
				"old_password" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "password field must not be blank.",
								"last" => true
						),
						"minLength" => array(
								"rule" => array("minLength", "8"),
								"message" => "Minimum 8 characters long.",
								"last" => true
						),
						"same_as_password" => array(
								"rule" => "checkPasswords",
								"message" => "this password does not match"
						)
				),
				"password" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "password field must not be blank.",
								"last" => true
						),
						"minLength" => array(
								"rule" => array("minLength", "8"),
								"message" => "Minimum 8 characters long.",
								"last" => true
						),
						"same_as_old_password" => array(
								"rule" => "newPassword",
								"message" => "this password already exits"
						)
				),
				"confirm_password" => array(
						"notBlank" => array(
								"rule" => "notEmpty",
								"message" => "confirm password field must not be blank.",
								"last" => true
						),
						"minLength" => array(
								"rule" => array("minLength", "8"),
								"message" => "Minimum 8 characters long.",
								"last" => true
						),
						"same_as_password" => array(
								"rule" => "equalToPassword",
								"message" => "Confirm password must be same as password."

						)
				),
		);
		$this->validate = $validatePassword;
		return $this->validates();
	}


	public function validateUser($data)
	{
		$emailAddress = array_shift($data);
		$password = $this->data["User"]["password"];
		$user = $this->find("first", array("conditions" => array("email_address" => $emailAddress)));
		if (!empty($user)) {
			$storedPassword = $user[$this->alias]["password"];
			$passwordHasher = new SimplePasswordHasher(array("hashType" => "sha256"));
			$password = $passwordHasher->hash($password);
			$correct = $password == $storedPassword;
			if ($correct) {
				return true;
			}
		}
		return false;
	}

	public function checkEmailAddress($data)
	{
		$emailAddress = array_shift($data);
		$count = $this->find("count", array("conditions" => array("email_address" => $emailAddress)));
		if ($count) {
			return true;
		}
		return false;
	}

	public function validateCode($data)
	{
		$verificationCode = array_shift($data);
		$count = $this->find("count", array("conditions" => array("verification_code" => $verificationCode)));
		if ($count) {
			return true;
		}
		return false;
	}
	public function newPassword($data)
	{
		$newPassword = array_shift($data);
		$password = $this->data["User"]["old_password"];
		if ($password == $newPassword) {
			return false;
		}
		return true;
	}

	public function checkPasswords($data)
	{
		$password = array_shift($data);
		$passwordHasher = new SimplePasswordHasher(array("hashType" => "sha256"));
		$password = $passwordHasher->hash($password);
//        $emailAddress = $this->data["User"]["email_address"];
		$conditions = array("password" => $password);
		$count = $this->find("count", array("conditions" => $conditions));
		if ($count) {
			return true;
		}
		return false;
	}

	public function equalToPassword($data)
	{
		$confirmPassword = array_shift($data);
		$password = $this->data["User"]["password"];
		if ($password === $confirmPassword) {
			return true;
		}
		return false;
	}

	public function getIdByEmailAddress($emailAddress)
	{

		if (!empty($emailAddress)) {
			$fields = array("User.id");
			$list = $this->find("list", array("conditions" => array("email_address" => $emailAddress), "fields" => $fields));
			return array_shift($list);
		}
		return null;
	}

	public function updateVerificationCodeByEmailAddress($emailAddress, $verificationCode)
	{
		if (!empty($emailAddress) && !empty($verificationCode)) {
			$fields = array("verification_code" => "'" . $verificationCode . "'");
			$conditions = array("email_address" => $emailAddress);
			return $this->updateAll($fields, $conditions);
		}
		return false;
	}

	public function updatePasswordByEmailAddress($emailAddress, $password)
	{
	//	print_r($emailAddress);
	//	print_r($password);

		if (!empty($emailAddress) && !empty($password)) {
			$fields = array("password" => "'" . $password . "'");
			$conditions = array("email_address" => $emailAddress);
			return $this->updateAll($fields, $conditions);
		}
		return false;
	}

	public function getProfileDataByUserId($userId)
	{
		if (!empty($userId)) {
			$conditions = array("user_id" => $userId);
			$result = $this->find("first", array("conditions" => $conditions));
			return $result;
		}
		return null;
	}

	public function getProfileImageById($userId)
	{
		$list = $this->find("list", array("conditions" => array("user_id" => $userId), "fields" => "Profile.avatar"));
		return array_shift($list);
	}


	public function beforeSave($options = array())
	{
		if (!empty($this->data["User"]["password"])) {
			$passwordHasher = new SimplePasswordHasher(array("hashType" => "sha256"));
			$this->data["User"]["password"] = $passwordHasher->hash($this->data["User"]["password"]);
		}
		return true;
	}

	public function bindNode($user) //if we want simplified per-group only permissions
	{
		$user = $this->find("first", array("conditions" => array("email_address" => $user["User"]), "fields" => array("User.group_id")));
		return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}
	public function createFromSocialProfile($incomingProfile){

		// check to ensure that we are not using an email that already exists
		$existingUser = $this->find('first', array(
				'conditions' => array('email_address' => $incomingProfile['SocialProfile']['email'])));

		if($existingUser){
			// this email address is already associated to a member
			return $existingUser;
		}

		// brand new user
		$socialUser['User']['email_address'] = $incomingProfile['SocialProfile']['email'];
		$socialUser['User']['auth_name'] = str_replace(' ', '_',$incomingProfile['SocialProfile']['display_name']);
		/* $socialUser['User']['role'] = 'bishop'; */// by default all social logins will have a role of bishop
		$socialUser['User']['password'] = date('Y-m-d h:i:s'); // although it technically means nothing, we still need a password for social. setting it to something random like the current time..
		$socialUser['User']['created'] = date('Y-m-d h:i:s');
		$socialUser['User']['modified'] = date('Y-m-d h:i:s');
		$socialUser['User']['group_id']=1;
		$socialUser['User']['is_active']=0;
		// save and store our ID
		$this->save($socialUser,false);
		$socialUser['User']['id'] = $this->id;

		return $socialUser;
	}
	function find_by_reset_password_code($hash)
	{
		print_r($hash);
		$result = $this->find('first', array('conditions' => array('User.tokenhash' => $hash)));
		return $result;
	}
}

