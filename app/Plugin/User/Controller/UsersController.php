<?php

/**
 * Created by IntelliJ IDEA.
 * User: Riyaz
 * Date: 9/16/14
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('CakeEmail', 'Network/Email');

class UsersController extends UserAppController
{
	public $name = "Users";
	public $layout = "user_layout";
	public $components = array('RequestHandler');
	public $uses = array("User.User", "User.UserContact", "User.UserAddress", "User.Country", "User.State", "User.City", "User.UserProperty", "User.Type", "User.Action", "User.PropertyList");

	public function contact_detail()
	{
		$this->layout = "admin_layout";
		if ($this->request->is('post')) {
			$userId = $this->Auth->user('id');
			$data = $this->request->data;
			$data["UserContact"]["user_id"] = $userId;
			$this->UserContact->set($data);
			$flag = $this->UserContact->validates(array('fieldList' => array('contact_1', 'contact_2')));
			if ($flag) {
				$contact_1 = $this->request->data['UserContact']['contact_1'];
				$contact_2 = $this->request->data['UserContact']['contact_2'];
				$contact_3 = $this->request->data['UserContact']['contact_3'];
				$this->UserContact->query("Update user_contacts set contact_1=$contact_1,contact_2=$contact_2,contact_3=$contact_3 where user_id=$userId");

				//$this->UserContact->save($this->request->data);
				//$this->Session->setFlash(__('The user has been Updated'));
				$this->redirect(array('controller' => 'users', 'action' => 'profile', 'plugin' => 'user'));

			} else {
				$this->Session->setFlash(__('The user could not be updated. Please, try again.'));
			}
		}
	}

	public function edit_contact()
	{
		$this->layout = "admin_layout";
		if ($this->request->is('post')) {
			$userId = $this->Auth->user('id');
			$data = $this->request->data;
			$data["UserContact"]["user_id"] = $userId;
			$this->UserContact->set('users', $data);
			$flag = $this->UserContact->validates(array('fieldList' => array('contact_1', 'contact_2')));
			if ($flag) {
				/*$contact_1=$this->request->data['UserContact']['contact_1'];
				$contact_2=$this->request->data['UserContact']['contact_2'];
				$contact_3=$this->request->data['UserContact']['contact_3'];
				$this->UserContact->query("Update user_contacts set contact_1=$contact_1,contact_2=$contact_2,contact_3=$contact_3 where user_id=$userId");*/

				$this->UserContact->save($this->request->data);
				//$this->Session->setFlash(__('The user has been Updated'));
				$this->redirect(array('controller' => 'users', 'action' => 'profile', 'plugin' => 'user'));

			} else {
				$this->Session->setFlash(__('The user could not be updated. Please, try again.'));
			}
		}
	}

	public function demo()
	{
	}

	public function register()
	{
		if ($this->request->is("ajax")) {
			$this->layout = "ajax";
		}

		if ($this->request->is("post")) {
			$this->layout = "ajax";
			$data = $this->request->data;
			$this->User->set($data);
			$flag = $this->User->validateNewUser();
			if ($flag) {
				$data["User"]["group_id"] = 2;
				$data["User"]["is_active"] = 1;
				$flag = $this->User->validates(array('fieldList' => array('email_address')));
				if ($flag) {
					$verification_code = substr(strtoupper(uniqid()), 0, 8);
					$user = $this->request->data;
					$email = new CakeEmail('gmail');
					$email->template('send_email')
							->emailFormat('html')
							->subject('"Registered Successfully"')
							->to($this->request->data['User']['email_address'])
							->from('no-reply@cakeapp.com')
							->viewVars(array('verification_code' => $verification_code, 'user' => $user))
							->send("<div>Dear User,</div><div>You have successfully registered with property Portel </div>");
					$this->User->save($this->request->data);
					$this->Session->setFlash(__('The user has been created.'));
					$this->redirect(array('controller' => 'home', 'action' => 'index', 'plugin' => null));
				} else {
					$this->Session->setFlash(__('The user could not be created. Please, try again.'));
				}
			}
		}
	}


	public function login()
	{
		if ($this->request->is("ajax")) {
			$this->layout = "ajax";
		}
		if ($this->request->is("post")) {
			$data = $this->request->data;
			$this->User->set($data);
			$flag = $this->User->validateExistingUser();
			if ($flag) {
				$flag = $this->Auth->login();
				$this->request->data = null;
				if ($flag) {

					$url = array("plugin" => "user", "controller" => "users", "action" => "dashboard");
					if ($this->request->is("ajax")) {
						echo "SUCCESS=" . implode("/", $url);
						exit;

					} else {
						$this->Session->setFlash(__('Welcome, ' . $this->Auth->user('username')));
						$this->redirect($this->Auth->redirectUrl());

					}
				}
			}
			$this->render("index");
		}
	}

	public function index()
	{
		//$this->layout = "home";
	}

	public function logout()
	{
		$this->redirect($this->Auth->logout());
	}

	public function dashboard()
	{
		$this->layout = "admin_layout";
		$sessionUser = $this->Session->read("Auth.User");
		if (empty($sessionUser)) {
			$url = array("controller" => "homes", "action" => "index", "plugin" => null);
			$this->redirect($url);
		}
		echo('welcome to profile');
	}


	public function change_password()
	{
		$this->layout = "admin_layout";
		if ($this->request->is("ajax")) {
			$this->layout = "ajax";
		}
		$sessionUser = $this->Session->read("Auth.User");
		if (empty($sessionUser)) {
			$url = array("controller" => "homes", "action" => "index", "plugin" => null);
			$this->redirect($url);
		}
		if ($this->request->is("post")) {
			$data = $this->request->data;
			$this->User->set($data);
			$fields = array("old_password", "password", "confirm_password");
			$flag = $this->User->validatePassword(array("fieldList" => $fields));
			if ($flag) {
				$emailAddress = $this->Session->read("Auth.User");
				$passwordHasher = new SimplePasswordHasher(array("hashType" => "sha256"));
				$password = $passwordHasher->hash($this->data["User"]["password"]);
				$flag = $this->User->updatePasswordByEmailAddress($emailAddress['email_address'], $password);
				if (!empty($flag)) {
					$this->request->data = null;
					$this->Session->write('email_address', null);
					$this->redirect("/user/users/change_password_success");
					exit;
				}

			}
		}
		$this->render("/Users/change_password");
	}

	public function change_password_success()
	{
		$this->layout = "home";
		$sessionUser = $this->Session->read("Auth.User");
		if (empty($sessionUser)) {
			$url = array("controller" => "homes", "action" => "index", "plugin" => null);
			$this->redirect($url);
		}
		if ($this->request->is("ajax")) {
			$this->layout = "ajax";
			$flag = $this->request->is("post");
			if ($flag) {
				$this->redirect("/user/users/index");
				exit;
			}
		}
		$this->render("/Users/change_password_success");
	}

//This function is used to recover lost password through email in application

	function remember_password()
	{
		$this->layout = "home";

		$this->User->recursive = -1;
		if (!empty($this->data)) {
			if (empty($this->data['User']['email'])) {
				$this->Session->setFlash('Please Provide Your Email Address that You used to Register with Us');
			} else {
				$email = $this->data['User']['email'];
				$fu = $this->User->find('first', array('conditions' => array('User.email_address' => $email)));
				if ($fu) {
					if ($fu['User']['status']) {
						$key = Security::hash(String::uuid(), 'sha512', true);
						$hash = sha1($fu['User']['username'] . rand(0, 100));
						$url = Router::url(array('controller' => 'users', 'action' => 'reset'), true) . '/' . $key . '#' . $hash;
						$ms = $url;
						$ms = wordwrap($ms, 1000);
						$fu['User']['tokenhash'] = $key;
						$this->User->id = $fu['User']['id'];
						if ($this->User->saveField('tokenhash', $fu['User']['tokenhash'])) {
							$email = new CakeEmail();
							$email->template('resetpw', 'default')
									->config('gmail')
									->emailFormat('html')
									->subject("Reset Your Password")
									->to($fu['User']['email_address'])
									->from('no-reply@cakeapp.com')
									->viewVars(array('ms' => $ms))
									->send();
							$this->Session->setFlash(__('Check Your Email To Reset your password', true));

							//============EndEmail=============//
						} else {
							$this->Session->setFlash("Error Generating Reset link");
						}
					} else {
						$this->Session->setFlash('This Account is not Active yet.Check Your mail to activate it');
					}
				} else {
					$this->Session->setFlash('Email does Not Exist');
				}
			}
		}
	}


	/**
	 * This function is used to reset lost password
	 */
	function reset($token = null)
	{
		$this->layout = "home";

		$this->User->recursive = -1;


		if (!empty($token)) {
			$u = $this->User->findBytokenhash($token);
			if ($u) {
				$this->User->id = $u['User']['id'];
				if (!empty($this->data)) {
					$this->User->data = $this->data;
					$this->User->data['User']['username'] = $u['User']['username'];
					$new_hash = sha1($u['User']['username'] . rand(0, 100));//created token
					$this->User->data['User']['tokenhash'] = $new_hash;
					if ($this->User->validates(array('fieldList' => array('password')))) {

						if ($this->User->save($this->User->data)) {
							$this->Session->setFlash('Password Has been Updated');
							$this->redirect(array('controller' => 'homes', 'action' => 'index', 'plugin' => ''));
						}
					} else {
						$this->set('errors', $this->User->invalidFields());
					}
				}
			} else {
				$this->Session->setFlash('Token Corrupted,,Please Retry.the reset link work only for once.');
			}
		} else {
			$this->redirect('/');
		}

	}

	public function address_detail($country_id = null)
	{
		$this->layout = "admin_layout";
		$country = $this->Country->find('list');
		$this->set('countries', $country);
		if ($this->request->is('post')) {
			$userId = $this->Auth->user('id');
			$data = $this->request->data;
			$data["UserAddress"]["user_id"] = $userId;
			$this->UserAddress->set($data);
			$flag = $this->UserAddress->validates(array('fieldList' => array('address', 'zip_code')));
			if ($flag) {
				$address = $this->request->data['UserAddress']['address'];
				$count = $this->request->data['UserAddress']['country_id'];
				$stat = $this->request->data['UserAddress']['state_id'];
				$cit = $this->request->data['UserAddress']['city_id'];
				$zip = $this->request->data['UserAddress']['zip_code'];
				$this->UserAddress->query("Update user_addresses set address=" . '"' . $address . '"' . ",country_id=$count,state_id=$stat,city_id=$cit,zip_code=$zip where user_id=$userId");

				//$this->UserAddress->save($this->request->data);
				//$this->Session->setFlash(__('The user has been Updated'));
				$this->redirect(array('controller' => 'users', 'action' => 'profile', 'plugin' => 'user'));

			} else {
				$this->Session->setFlash(__('The user could not be updated. Please, try again.'));
			}
		}
	}

	function get_states($country_id = null)
	{
		$this->layout = 'ajax';
		$this->autoRender = false;
		$states = $this->State->find('list', array('conditions' => array('State.country_id' => $country_id)));
		return (json_encode($states));
	}

	function get_cities($state_id = null)
	{
		$this->layout = 'ajax';
		$this->autoRender = false;

		$cities = $this->City->find('list', array('conditions' => array('City.state_id' => $state_id)));
		return (json_encode($cities));
	}

	public function add_property()
	{
		$this->layout = 'admin_layout';
		$country = $this->Country->find('list');
		$this->set('countries', $country);
		$property = $this->Type->find('list');
		$this->set('property_types', $property);
		$action = $this->Action->find('list');
		$this->set('actions', $action);
		if ($this->request->is('post')) {
			$userId = $this->Auth->user('id');
			$data = $this->request->data;
			$data["UserProperty"]["user_id"] = $userId;
			$this->UserProperty->set($data);
			$flag = $this->UserProperty->validates(array());
			if ($flag) {
				$this->UserProperty->save($this->request->data);
				$this->Session->setFlash(__('The user has been Updated'));
				$this->redirect(array('controller' => 'users', 'action' => 'dashboard', 'plugin' => 'user'));
			} else {
				$this->Session->setFlash(__('The user could not be updated. Please, try again.'));
			}
		}
	}

	public function profile()
	{
		$this->layout = 'admin_layout';
		$user_id = $this->Auth->user('id');
		$usersId = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
		$this->set('users', $usersId);

		$country_id = $usersId['UserAddress']['country_id'];
		$this->Country->recursive = -1;
		$country = $this->Country->find('first', array('conditions' => array('Country.id' => $country_id)));
		$country = $country['Country']['name'];
		$this->set('country', $country);

		$state_id = $usersId['UserAddress']['state_id'];
		$state = $this->State->find('first', array('conditions' => array('State.id' => $state_id)));
		$state = $state['State']['name'];
		$this->set('state', $state);

		$city_id = $usersId['UserAddress']['city_id'];
		$city = $this->City->find('first', array('conditions' => array('City.id' => $city_id)));
		$city = $city['City']['name'];
		$this->set('city', $city);


		$this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
	}

	public function property_list()
	{
		$this->layout= 'admin_layout';
		$this->paginate = array(
				'limit' => 100);
		$property = $this->paginate('PropertyList');
		$this->set(compact('property'));


		//print_r($property);
		$temp = array();
		foreach ($property as $prop) {
			$temp[] = $prop['PropertyList']['property_type_id'];
			$temp[] = $prop['PropertyList']['action_id'];
print_r($temp);
		}
		$property_type = $this->Type->find('all', array('conditions' => array('Type.id IN' => $temp),'fields'=>array('name')));
		$temp = array();
		foreach ($property_type as $prop) {
			$temp[] = $prop['Type']['name'];

		}
 print_r($temp);
		$this->set('property', $temp);
	}
}
