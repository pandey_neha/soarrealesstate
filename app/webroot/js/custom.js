//used in following layouts
//Plugin/Plan/View/Layouts/plan_category_layout.ctp
//Plugin/Plan/View/Layouts/plan_layout.ctp
//Plugin/Support/View/Layouts/help_layout.ctp
//Plugin/Support/View/Layouts/sub_layout.ctp
//View/Layouts/default.ctp
//var HOST = baseUrl;
//var HOST = 'http://dropang.soarlogic.com/'
/*var HOST = "/";*/
var HOST="http://localhost/realestate/"
jQuery(document).ready(function () {
	// fade in #back-top
	if (jQuery('#modal-static').length > 0) {
		jQuery('#modal-static').on('show.bs.modal', function (event) {
			var url = jQuery(event.relatedTarget).data('url');
			getForm(url, this.id);
		});
	}
});

var CommonForm = function () {
};

getForm = function (url, id) {
	jQuery.get(HOST + url, {}, function (responseText) {
		jQuery("#" + id).html(responseText);
		jQuery(document).ready(function () {
			var formObject = jQuery("form#commonForm");
			if (formObject.length > 0) {
				formObject.submit(function (event) {
					event.preventDefault();
					var commonForm = new CommonForm();
					commonForm.submit(formObject);
				});
			}
		});
	});
};

CommonForm.prototype.submit = function (formObject) {

	var url = 'user/users/login'
			, method = formObject.attr('method')
			, data = new FormData(formObject[0])
			, button = formObject.find('input[type=submit]');
	button.attr('disabled', 'disabled');
	//alert(url);
	jQuery.ajax({
		url: url,
		type: 'post',
		data: data,
		dataType: 'html',
		cache: false,
		contentType: false,
		processData: false,
		beforeSend: function () {
			//jQuery.blockUI({ message: jQuery('#loaderMessage') });   //used for loader
		},
		statusCode: {
			200: function (data) {
				commonFormProcessData(data, formObject);
			},
			500: function () {
				//handle error
			},
			404: function () {
				// handle error
			}
		},
		complete: function () {
			button.removeAttr('disabled');
			//jQuery.unblockUI();
		}
	})
};

commonFormProcessData = function (data, $el) {

	if (data.indexOf("SUCCESS") > -1) {
		data = data.replace("SUCCESS=", "");
		location.href = (HOST + data);
	} else {
		jQuery("#modal-static").html(data);
		jQuery(document).ready(function () {
			var formObject = jQuery("form#commonForm");
			if (formObject.length > 0) {
				formObject.submit(function (event) {
					event.preventDefault();
					var commonForm = new CommonForm();
					commonForm.submit(formObject);
				});
			}
		});
	}
};



jQuery(document).ready(function () {
	jQuery(document).ajaxStart(function () {
		jQuery("#wait").css("display", "block");
	});
	jQuery(document).ajaxComplete(function () {
		jQuery("#wait").css("display", "none");
	});
});
