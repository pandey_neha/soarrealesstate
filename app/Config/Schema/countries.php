<?php
/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 10/16/14
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */
App::uses('ClassRegistry', 'Utility');

class CountriesSchema extends CakeSchema
{

    public $name = 'Countries';

    public function before($event = array())
    {
        $db = ConnectionManager::getDataSource($this->connection);
        $db->cacheSources = false;
        return true;
     }

    public function after($event = array())
    {
        if (isset($event['create'])) {
            $table = $event['create'];
            $data = null;
            switch($table) {
                case 'countries':
                    $this->insertDefaultCountries();
                    break;
            }
        }
    }

    public $countries = array(
        'id' => array('type' => 'integer', 'null' => false, 'key' => 'primary', 'length' => 10, "default" => 0,"unsigned" => true),
        'iso' => array('type' => 'string', 'null' => false, 'length' => 45),
        'iso3' => array('type' => 'string', 'null' => false, 'length' => 45),
        'fips' => array('type' => 'string', 'null' => false, 'length' => 45),
        'name' => array('type' => 'string', 'null' => false, 'unique' => true,'length' => 255),
        'continent' => array('type' => 'string', 'null' => false, 'length' => 255),
        'currency_code' => array('type' => 'string', 'null' => false, 'length' => 45),
        'phone_prefix' => array('type' => 'string', 'null' => false, 'length' => 45),
        'languages' => array('type' => 'string', 'null' => false, 'length' => 45),
        'geo_name_id' => array('type' => 'string', 'null' => false, 'length' => 45),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => true),
            'geo_name_id' => array('column' => 'geo_name_id'))

    );
    public function insertDefaultCountries(){
        $country = ClassRegistry::init("Country");
        $records = array(
            array(
                "Country" => array(
                    "id" => "1",
                    "iso" =>"",
                    "iso3" =>"",
                    "fips" =>"",
                    "name" => "India",
                    "continent" =>"Asia",
                    "currency_code" =>"INR",
                    "phone_prefix" =>"0091",
                    "languages" =>"",
                    "geo_name_id" =>"",

                )
            ),
            array(
                "Country" => array(
                    "id" => "2",
                    "iso" =>"",
                    "iso3" =>"",
                    "fips" =>"",
                    "name" => "Australia",
                    "continent" =>"Australia",
                    "currency_code" =>"AUD",
                    "phone_prefix" =>"0061",
                    "languages" =>"",
                    "geo_name_id" =>"",
                )
            ),
        );
        $country->saveAll($records);
    }
}

