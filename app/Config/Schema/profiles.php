<?php
/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 10/16/14
 * Time: 11:28 AM
 * To change this template use File | Settings | File Templates.
 */

class ProfilesSchema extends CakeSchema {

    public $name = 'Profiles';

    public function before($event = array()) {
        return true;
    }

    public function after($event = array()) {
    }

    public $profiles = array(
        'id' => array('type' => 'integer', 'null' => false, 'key' => 'primary','length' =>10,"default" =>0,"unsigned" =>true),
        'user_id' => array('type' => 'integer', 'null' => false,'length' =>10,"default" => 0,"unsigned" => true),
        'profile_image' => array('type' => 'string','null' =>false),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => true),
            'user_id' => array('column' => 'user_id')),
    );
}



