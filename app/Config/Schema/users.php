<?php
/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 10/16/14
 * Time: 12:00 PM
 * To change this template use File | Settings | File Templates.
 */

App::uses('ClassRegistry', 'Utility');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');


class UsersSchema extends CakeSchema {

    public $name = 'Users';

    public function before($event = array()) {
        $db = ConnectionManager::getDataSource($this->connection);
        $db->cacheSources = false;
        return true;
    }

    public function after($event = array()) {
        if (isset($event['create'])) {
            $table = $event['create'];
            $data = null;
            switch($table) {
                case 'users':
                    $this->insertDefaultUsers();
                    break;
            }
        }
    }

    public $users = array(
        'id' => array('type' => 'integer', 'null' => false, 'key' => 'primary', 'length' =>10, "default" => 0, "unsigned" => true),
        'email_address' => array('type' => 'string', 'null' => false, 'length' =>255),
        'password' => array('type' => 'string', 'null' => false, 'length' =>100),
        'is_active' =>array('type' => "boolean",'null' =>false, 'length'=> 1, "unsigned" => true),
        'verification_code' => array('type' => 'string', 'length' => 100, 'null' => true),
        'auth_id' => array('type' => 'string', 'null' => true),
        'auth_name' => array('type' => 'string', 'null' => true),
        'created' => array('type' => 'datetime', 'null' => false),
        'modified' => array('type' => 'datetime', 'null' => false),
        'group_id' => array('type' => 'boolean', 'null' => false, 'length' =>3, "unsigned" => true),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => true),
            'foreign' => array('column' => 'group_id', "references" => "groups"),
            'auth_id' => array('column' => 'auth_id'),
            'group_id' => array('column' => 'group_id'),
            'email_address' => array('column' => 'email_address', "unique" => true)
        ),
        'tableParameters' => array(
            'engine' => 'InnoDB',
            'charset' => 'utf8',
            'collate' => 'utf8_general_ci'
        )
    );
    public function insertDefaultUsers(){
        $user = ClassRegistry::init("User");
        $passwordHasher = new SimplePasswordHasher(array("hashType" => "sha256"));
        $password = $passwordHasher->hash("12345678");
        $records = array(
            array(
                "User" => array(
                    "email_address" => "admin@soarlogic.com",
                    "password" => $password,
                    "is_active" => true,
                    "verification_code" => null,
                    "auth_id" => null,
                    "created" => date("Y-m-d H:i:s"),
                    "modified" => date("Y-m-d H:i:s"),
                    "group_id" => 1,
                )
            ),
            array(
                "User" => array(
                    "email_address" => "admin@soarlogic.com",
                    "password" => $password,
                    "is_active" => true,
                    "verification_code" => null,
                    "auth_id" => null,
                    "created" => date("Y-m-d H:i:s"),
                    "modified" => date("Y-m-d H:i:s"),
                    "group_id" => 2,

                )
            ),
        );
        $user->saveAll($records);
    }
}
