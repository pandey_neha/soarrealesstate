<?php
/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 10/16/14
 * Time: 11:21 AM
 * To change this template use File | Settings | File Templates.
 */

class GroupsSchema extends CakeSchema {

    public $name = 'Groups';

    public function before($event = array()) {
        $db = ConnectionManager::getDataSource($this->connection);
        $db->cacheSources = false;
        return true;
    }

    public function after($event = array()) {
        if (isset($event['create'])) {
            $table = $event['create'];
            switch($table) {
                case 'groups':
                    $this->insertDefaultGroups();
                    break;
            }
        }
    }

    public $groups = array(
        'id' => array('type' => 'integer', 'null' => false, 'key' => 'primary', 'length' =>10, "unsigned" => true),
        'name' => array('type' => 'string', 'null' => false, "length" => 100),
        'created' => array('type' => 'datetime', 'null' => false),
        'modified' => array('type' => 'datetime', 'null' => false),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => true),
            'name' => array('column' => 'name', "unique" => true)
        ),
        'tableParameters' => array(
            'engine' => 'InnoDB',
            'charset' => 'utf8',
            'collate' => 'utf8_general_ci'
        )

    );
    public function insertDefaultGroups(){
        $group = ClassRegistry::init("Group");
        $records = array(
            array(
                "Group" => array(
                    "name" => "Administrator",
                    "created" => date("Y-m-d H:i:s"),
                    "modified" => date("Y-m-d H:i:s"),
                )
            ),
            array(
                "Group" => array(
                    "name" => "User",
                    "created" => date("Y-m-d H:i:s"),
                    "modified" => date("Y-m-d H:i:s"),
                )
            )
        );
        $group->saveAll($records);
    }
}


