<?php
/**
 * Created by IntelliJ IDEA.
 * User: OM SAI
 * Date: 11/11/14
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */

class SocialProfilesSchema extends CakeSchema
{

    public $name = 'SocialProfiles';

    public function before($event = array())
    {
        return true;
    }

    public function after($event = array())
    {
    }

    public $social_profiles = array(
        'id' => array('type' => 'integer', 'null' => false, 'key' => 'primary', 'length' => 10, "unsigned" => true),
        'user_id' => array('type' => 'integer', 'length' => 10, "default" => null, "unsigned" => true),
        'social_network_id' => array('type' => 'string', "default" => null,'length' =>128),
        'social_network_name' => array('type' => 'string', "default" => null,'length' =>64),
        'email' => array('type' => 'string', 'null' => false,'length' =>128),
        'display_name' => array('type' => 'string', 'null' => false,'length' =>128),
        'first_name' => array('type' => 'string', 'null' => false,'length' =>128),
        'last_name' => array('type' => 'string', 'null' => false,'length' =>128),
        'link' => array('type' => 'string', 'null' => false,'length' =>512),
        'picture' => array('type' => 'string', 'null' => false,'length' =>512),
        'created' => array('type' => 'datetime',  "default" => null),
        'modified' => array('type' => 'datetime', "default" => null),
        'status' => array('type' => 'boolean', 'null' => false, 'length' => 1, "default" => 1, 'unsigned' => true),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => true),
        ),
        'tableParameters' => array(
            'engine' => 'InnoDB',
            'charset' => 'utf8',
            'AUTO_INCREMENT' =>'19',
            'collate' => 'utf8_general_ci'
        )
    );
}