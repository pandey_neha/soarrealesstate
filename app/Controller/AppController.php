<?php
/**
 * Configlication level Controller
 *
 * This file is Configlication-wide controller file. You can put all
 * Configlication-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Config.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Configlication Controller
 *
 * Add your Configlication-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        Config.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-Config-controller
 */

App::uses("Controller", "Controller");
/*App::uses("AclComponent", "Controller/Component");*/
App::uses("AuthComponent", "Controller/Component");

class AppController extends Controller
{

	public $helpers = array("Session", "Form", "Html");

	public $components = array(
			"RequestHandler",
			"Session",
			"Security" => array(
					"csrfExpires" => "+30 mins",
					'csrfUseOnce' => false
			),
			'Auth' => array(
					'authenticate' => array(
							'Form' => array(
									'fields' => array('username' => 'email_address', 'password' => 'password'),
									'userModel' => 'User.User',
									'scope' => array('User.is_active' => 1),
									'passwordHasher' => array(
											'className' => 'Simple',
											'hashType' => 'sha256'
									)
							)
					),
					'loginAction' => array(
							'controller' => 'users', 'action' => 'index', 'plugin' => "user"),
					'loginRedirect' => array(
							'controller' => 'users', 'action' => 'dashboard', 'plugin' => "user"),
					'logoutRedirect' => array(
							'controller' => 'homes', 'action' => 'index', 'plugin' =>""),
					'autoRedirect' => false,
					'authError' => 'Did you really think you are allowed to see that?',
			)
	);

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Security->blackHoleCallback = 'blackhole';
		$this->Auth->allow("logout","index", "about", "agent", "blog", "blog_detail", "contact", "buy","sale","rent","register","property_detail","dashboard","login","forgot_password","verify_code","remember_password","reset","contact_detail","address_detail");
		$_SESSION['sessionTest'] = "Hellow World!"; // Do not remove this line, it is affecting on the $_SESSION variable in webroot's config file
		if ($this->Session->check('Config.language')) {
			Configure::write('Config.language', $this->Session->read('Config.language'));
		}

	}
	public function blackhole($get) {
// handle errors.
	}
}
